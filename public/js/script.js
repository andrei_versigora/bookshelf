// Inserire qui tutto il codice Javascript

$(".delete").click(function () {
	var href = $(this).attr('href');
	BootstrapDialog.confirm("Sei sicuro di voler eliminare questo prodotto di ricerca?", function(result) {
        if (result) {
        	window.location = href;
    	}
    });
	return false;
});

$("#seleziona-autori").multipleSelect({
	filter: true,
	selectAll: false
});