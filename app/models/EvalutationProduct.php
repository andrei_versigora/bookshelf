<?php 

/**
 * Questa classe descrive una tripla formata da un prodotto di ricerca, il ricercatore
 * che lo ha sottomesso al processo di valutazione e il periodo di valutazione per il
 * quale è stato sottomesso.
 */
class EvalutationProduct extends Eloquent  {
	
	// Nome della tabella del DB mappata da questa classe
	protected $table = 'lista_prodotti_per_valutazione';

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;

	// Imposta la chiave primaria. Richiesta perchè attualmente la chiave primaria
	// nella tabella è "ID" (in maiuscolo) invece che "id" (minuscolo).
	protected $primaryKey = "id";

    public function prodottiDiRicerca()
    {
        return $this->belongsToMany('ResearchProduct', 'inserimento', 'id_lista_prodotti_per_valutazione', 'id_prodotto_di_ricerca')
        	->withPivot('posizione');
    }

	// ATTENZIONE!
	// La tabella non contiene il campo "id_prodotto_di_ricerca"
	public function prodotti()
	{
		return $this->hasMany('ResearchProduct','id' , 'Inserimento' ,'id_lista_prodotti_per_valutazione');
	}

	public function ricercatore()
	{
		return $this->hasOne('Utente', 'id_ricercatore');
	}

	public function periodo()
	{
		return $this->hasOne('EvalutationPeriod', 'id_periodo_valutazione');
	}

	/**
	 * Questo metodo restituisce la lista di prodotti scelti per la valutazione
	 * dal ricercatore con id $idRicercatore per il periodo di valutazione 
	 * identificato con $idPeriodo.
	 * Se l'id del ricercatore non è presente, saranno restitite le liste
	 * di tutti i ricercatori per quello specifico periodo.
	 *
	 * @param $idPeriodo int
	 * @param $idRicercatore int
	 *
	 * @return EvalutationProduct[]  
	 */
	public static function listaProdottiDaValutare($idPeriodo, $idRicercatore = null)
	{
		//$query = EvalutationProduct::where('id_periodo_valutazione', '=', $idPeriodo);

		$query = \EvalutationProduct::query()
			->leftJoin('inserimento', 'lista_prodotti_per_valutazione.id', '=', 'inserimento.id_lista_prodotti_per_valutazione')
			->leftJoin('utente','utente.id','=','id_ricercatore')
			->leftJoin('prodotto_di_ricerca', 'prodotto_di_ricerca.id', '=', 'inserimento.id_prodotto_di_ricerca');

		$query = $query->where('id_periodo_valutazione', '=', $idPeriodo);

		if ($idRicercatore != null) {
			$query = $query->where('id_ricercatore', '=', $idRicercatore);
		}

		
		return $query;
	}

}