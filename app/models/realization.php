<?php 

/**
 * Questa classe permette di interagire con la tabella "realizzazione"
 * della base di dati e descrive la partecipazione alla realizzazione
 * di un autore ad un determinato prodotto di ricerca.
 */
	class realization extends Eloquent
	{
		protected $table="realizzazione";
		public $timestamps=false;
	}
?>