<?php  

/**
 * Questa classe permette di interagire con la tabella "edizione_critica_traduzione_commento"
 * della base di dati ed è una classe figlia di ResearchProductDetails.
 */
class CriticalEditionTranslationAndScientificCommentary extends ResearchProductDetails
{
	protected $table="edizione_critica_traduzione_commento";
}
