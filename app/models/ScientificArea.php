<?php  

/**
 * Questa classe permette di interagire con la tabella "area_scientifica"
 * della base di dati e rappresenta un'area scientifica.
 */
class ScientificArea extends Eloquent
{
	protected $table = "area_scientifica";
	public $timestamps = false;
	protected $primaryKey="id";

	/**
     * Definisce la relazione 1aN tra il ricercatore e l'area scientifica.
     * Un ricercatore appartiene ad una sola area scientifica.      
	 */
	function researchers(){
		return $this->hasMany("ricercatore","id_area_scientifica");
	}

}
