<?php  

/**
 * Questa classe permette di interagire con la tabella "articolo_su_rivista"
 * della base di dati ed è una classe figlia di ResearchProductDetails.
 */
class JournalArticle extends ResearchProductDetails
{
	protected $table="articolo_su_rivista";
}
