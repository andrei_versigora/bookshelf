<?php

/**
 * Questa classe permette di interagire con la tabella "prodotto_di_ricerca"
 * della base di dati e rappresenta un'astrazione di un generico prodotto di ricerca.
 */
class ResearchProduct extends Eloquent
{

	// Stati di validazione di un prodotto di ricerca
	const NON_VALIDATO = 'non_validato';
	const IN_VALIDAZIONE = 'in_validazione';
	const VALIDATO_DIP = 'validato_dipartimento';
	const VALIDATO = 'validato_area';

		
	// Nome della tabella del DB mappata da questa classe
	protected $table = 'prodotto_di_ricerca';

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;

	// Imposta la chiave primaria. Richiesta perchè attualmente la chiave primaria
	// nella tabella è "ID" (in maiuscolo) invece che "id" (minuscolo).
	protected $primaryKey = "id";

	/**
	 * Questo metodo verifica che il ricercatore avente $idRicercatore come
	 * id abbia i permessi di scrittura sul prodotto con id $idProdotto.
	 *
	 * @param $idRicercatore int
	 * @param $idProdotto int
	 *
	 * @return bool True se il ricercatore ha i permessi, false altrimenti
	 */
	public static function verificaPermessi($idRicercatore, $idProdotto)
	{
		$prodotto = ResearchProduct::find($idProdotto);
		if (empty($prodotto)) {
			return false;
		}

		$ricercatore = $prodotto->ricercatori()
			->where('id_ricercatore', '=', $idRicercatore)
			->where('ha_permessi', '=', '1')->first();

		return (!empty($ricercatore));
	}


	public function ricercatori()
	{
		return $this->belongsToMany('utente', 'realizzazione', 'id_prodotto_di_ricerca', 'id_ricercatore')
			->withPivot('ha_permessi');
	}

	public function prodottiDaValutare()
	{
		return $this->belongsToMany('lista_prodotti_per_valutazione', 'inserimento', 'id_prodotto_di_ricerca', 'id_lista_prodotti_per_valutazione')
			->withPivot('posizione');
	}

	public function dettagli()
	{
		$tipologia = 'Other';
		switch ($this->tipologia_del_prodotto) {
			case 'articolo_su_rivista':
				$tipologia = 'JournalArticle';
				break;
			case 'libro_capitolo':
				$tipologia = 'BookChapter';
				break;
			case 'atto_di_congresso':
				$tipologia = 'ActOfCongress';
				break;
			case 'edizione_critica':
			case 'traduzione':
			case 'commento':
				$tipologia = 'CriticalEditionTranslationAndScientificCommentary';
				break;
		}
		return $this->hasOne($tipologia, 'id_prodotto_di_ricerca');
	}

	/**
	 * 
	 *
	 * @param $idAreaScientifica int
	 * @param $tipologia string
	 * @param $autore string
	 * @param $idAutore int
	 * @param $titolo string
	 *
	 * @return bool True se il ricercatore ha i permessi, false altrimenti
	 */
	public static function cerca($idAreaScientifica = null, $tipologia = null, $autore = null, $idAutore = null, $titolo = null, $Start_data = null, $End_data = null)
	{
		// Dio salvaci da questa query... =(

		$query = \ResearchProduct::query()
			->leftJoin('realizzazione', 'prodotto_di_ricerca.id', '=', 'realizzazione.id_prodotto_di_ricerca')
            ->leftJoin('utente', 'utente.ID', '=', 'realizzazione.id_ricercatore');

		// Per area scientifica
		if (!empty($idAreaScientifica)) {
            $query = $query->where('utente.id_area_scientifica', '=', $idAreaScientifica);
		}

		// Per Tipologia
		if (!empty($tipologia)) {
			$query = $query->where('tipologia_del_prodotto', '=', $tipologia);
		}

		// Per Autore
		if (!empty($autore)) {
			$query = $query->select(
            		'*',
            		\DB::raw('concat(utente.Nome, " ", utente.Cognome) AS NomeCognome'))

            	// Per adesso funziona solo con il cognome
            	// TO DO
				->where('utente.Cognome', 'LIKE', "%$autore%");
		}


		// Per autore ID
		if (!empty($idAutore)) {
			$query = $query->where('utente.ID', '=', $idAutore);
		}


		// Per Data
		if (!empty($Start_data)) {
			$query = $query->where('data_pubblicazione', '>=', $Start_data);
		}
		
		if (!empty($End_data)) {
			$query = $query->where('data_pubblicazione', '<=', $End_data);
		}
		
		// Per titolo del prodotto di ricerca
		if (!empty($titolo)) {
			$query = $query->where('titolo_del_prodotto_di_ricerca', 'LIKE', "%$titolo%");
		}

		$query = $query->select(['prodotto_di_ricerca.id','prodotto_di_ricerca.titolo_del_prodotto_di_ricerca'])->distinct();
		return $query;
	}

}
