<?php

/**
 * Questa classe permette di interagire con la tabella "periodo_validazione"
 * della base di dati e rappresenta un periodo di validazione.
 */
class ValidationPeriod extends Eloquent {
	protected $table = 'periodo_validazione';

	protected $primaryKey = "id";

	public $timestamps = false;

}