<?php 

/**
 * Questa classe permette di interagire con la tabella "periodo_valutazione"
 * della base di dati e rappresenta un periodo di valutazione.
 */
class EvalutationPeriod extends Eloquent  {
	
	// Nome della tabella del DB mappata da questa classe
	protected $table = 'periodo_valutazione';

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;

	// Imposta la chiave primaria. Richiesta perchè attualmente la chiave primaria
	// nella tabella è "ID" (in maiuscolo) invece che "id" (minuscolo).
	protected $primaryKey = "id";

	/**
	 * Questo metodo restituisce il periodo di valutazione valido per la
	 * data $data. Restituisce, se presente, il periodo di valutazione che inizia
	 * prima di $data e termina dopo $data.
	 *
	 * @param $data
	 * @return EvalutationPeriod
	 */
	public static function caricaPeriodoValutazionePerData($data)
	{
		$periodoAttuale=\EvalutationPeriod::where('data_inizio','<=',$data)->where('data_fine','>=',$data)->first();

		return $periodoAttuale;
	}

	public static function caricaPeriodoValutazioneAttuale()
	{
		$time=time();
		$today=date("Y-m-d",$time);

		return EvalutationPeriod::caricaPeriodoValutazionePerData($today);
	}

}