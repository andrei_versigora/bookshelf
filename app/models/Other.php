<?php  

/**
 * Questa classe permette di interagire con la tabella "altro"
 * della base di dati ed è una classe figlia di ResearchProductDetails.
 */
class Other extends ResearchProductDetails
{
	protected $table="altro";
}
