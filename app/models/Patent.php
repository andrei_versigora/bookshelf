<?php  

/**
 * Questa classe permette di interagire con la tabella "brevetto"
 * della base di dati ed è una classe figlia di ResearchProductDetails.
 */
class Patent extends ResearchProductDetails
{
	protected $table="brevetto";
}
