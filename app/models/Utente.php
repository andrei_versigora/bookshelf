<?php 

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Questa classe permette di interagire con la tabella "utente"
 * della base di dati e rappresenta un utente interno o esterno al sistema.
 */
class Utente extends Eloquent implements UserInterface  {

	// Tipologie di utenti presenti nel sistema:
	const AMMINISTRATORE 	= 'amministratore';
	const RICERCATORE 		= 'ricercatore';
	const RES_DIPARTIMENTO 	= 'resp.dipartimento';
	const RES_DISCIPLINARE 	= 'resp.disciplinare';
	const DIR_DISCIPLINARE 	= 'dir.disciplinare';
	const RES_ATENEO 		= 'res.ateneo';
	
	// Nome della tabella del DB mappata da questa classe
	protected $table = 'utente';

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;

	// Imposta la chiave primaria. Richiesta perchè attualmente la chiave primaria
	// nella tabella è "ID" (in maiuscolo) invece che "id" (minuscolo).
	protected $primaryKey = "id";

	public function prodotti()
    {
        return $this->belongsToMany('ResearchProduct', 'realizzazione', 'id_ricercatore', 'id_prodotto_di_ricerca')
        	->withPivot('ha_permessi');
    }

    public function prodottiConPermessi()
    {
        return $this->prodotti()->where('ha_permessi', '=', '1');
    }

    public static function ricercatori()
    {
    	return Utente::where('tipologia', '=', Utente::RICERCATORE);
    }

	// I seguenti metodi sono la definizione dell'interfaccia "UserInterface"
	// utilizzata per il sistema di login/logout.

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	public function evalutationSubmitList()
	{
		return EvalutationProduct::where('id_ricercatore','=',$this->getKey())->first();
	}

}