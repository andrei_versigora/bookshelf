<?php

/**
 * Questa classe permette di collegare un prodotto specifico all'astrazione corrispondente di ResearchProduct.
 */
class ResearchProductDetails extends Eloquent {

	protected $primaryKey = "id_prodotto_di_ricerca";

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;

	public function prodotto()
    {
        return $this->belongsTo('ResearchProduct', 'id_prodotto_di_ricerca');
    }

}