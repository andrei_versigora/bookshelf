<?php  
/**
 * Questa classe permette di interagire con la tabella "libro_capitolo"
 * della base di dati ed è una classe figlia di ResearchProductDetails.
 */
class BookChapter extends ResearchProductDetails
{
	protected $table="libro_capitolo";
}