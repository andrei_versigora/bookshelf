<?php

/**
 *	Questa classe descrive la relazione che collega i prodotti appartenenti ad una lista di valutazione
 *
 *
 **/

class Submit extends Eloquent	{
	// Nome della tabella del DB mappata da questa classe
	protected $table = 'inserimento';

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;

	// Imposta la chiave primaria. Richiesta perchè attualmente la chiave primaria
	// nella tabella è "ID" (in maiuscolo) invece che "id" (minuscolo).
	protected $primaryKey = "id_prodotto_di_ricerca";

}