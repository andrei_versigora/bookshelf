<?php 

/**
 * Questa classe permette di interagire con la tabella "inserimento"
 * della base di dati e rappresenta i prodotti da valutare
 */
class insertion extends Eloquent
{
	// Nome della tabella del DB mappata da questa classe
	protected $table = "inserimento";

	// Dichiara che la tabella non utilizza i campi "created_at" e "updated_at"
	public $timestamps = false;


}