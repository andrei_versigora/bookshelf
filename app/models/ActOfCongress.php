<?php

/**
 * Questa classe permette di interagire con la tabella "atto_di_congresso"
 * della base di dati ed è una classe figlia di ResearchProductDetails.
 */
class ActOfCongress extends ResearchProductDetails
{
	protected $table="atto_di_congresso";
}