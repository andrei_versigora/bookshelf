@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Valutazione <small>Periodi valutazione</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
          			<li class="active">Periodi validazione</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Periodi valutazione</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Data Inizio</th>
							<th class="header">Data Fine</th>
							<th class="header">Anno Inizio</th>
							<th class="header">Anno Fine</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($periodi as $element)
							<tr>
							<td>{{$element->data_inizio}}</td><td>{{$element->data_inizio}}</td><td>{{$element->anno_inizio}}</td><td>{{$element->anno_fine}}</td>
							</tr>
						@endforeach
						
					</tbody>
				</table>
			</div>
			</div>
			</div>
			</div>
@stop