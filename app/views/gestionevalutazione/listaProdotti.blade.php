@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Valutazione <small>lista prodotti valutazione</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
          			<li class="active">Lista valutazione</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Lista prodotti valutazione</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Titolo del prodotto</th>
							<th class="header">Descrizione</th>
							<th class="header">Autori</th>
							<th class="header">Tipologia del prodotto</th>
							<th class="header">Azione</th>
						</tr>
					</thead>
					<tbody> <?php $count=0;?>
			@foreach ($prodotti as $prodotto)
				<tr id= <?php echo "$count" ?> >
					<td hidden="true">{{{$prodotto->id}}}</td>
					<td>{{{ $prodotto->titolo_del_prodotto_di_ricerca }}}</td>
					<td>{{{ $prodotto->descrizione }}}</td>
					<td>
						<ul>
						@foreach ($prodotto->ricercatori as $ricercatore)
							<li>{{{ $ricercatore->cognome }}} {{{ $ricercatore->nome }}}</li>
						@endforeach
						</ul>
					</td>
					<td>{{$prodotto->tipologia_del_prodotto}}</td>
					<td>
						<button onclick="Select(this)">Seleziona</button>
					</td>
				</tr> <?php $count++; ?>
			@endforeach
					</tbody>
				</table>
				{{Form::open(array(
						'action' => 'GestioneValutazione\EvalutationProductSelectionController@submit',
						'method' => 'POST'
				))}}

			</div>
			</div>
			</div>
			</div>

<div class="row">

<div class="panel panel-primary">
<div class="panel-heading"><i class="fa fa-tasks"></i> Prodotti Scelti</div>
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="header">Titolo del prodotto</th>
				<th class="header">Descrizione</th>
				<th class="header">Autori</th>
				<th class="header">Tipologia del prodotto</th>
				<th class="header">Azione</th>
			<tr>
		</thead>
		
		<tbody id="corpoTabella2">

		</tbody>
		
	</table>
	{{Form::submit()}}
		{{Form::close()}}
			</div>
		

		</div>

	</div>

</div>

	<script type="text/javascript">
	count=0;
	function Select(object)
	{
		count++;
		object=object.parentNode;
		object=object.parentNode;
		var tr=document.createElement('TR');
		tr.id=object.id;
		var td=document.createElement('TD');
		var table=document.getElementById('corpoTabella2');
		father=table.appendChild(tr);
		child=father.appendChild(td);
		child.hidden=true;
		var input=document.createElement('INPUT');
		input.type="text";
		child=child.appendChild(input);
		child.value=object.children[0].innerHTML;
		child.name="P"+count;

		for(i=1;i<5;i++){
			td=document.createElement('TD')
			input=document.createElement('INPUT');
			input.type="text";
			input.readOnly=true;
		var temp=object.children[i].innerHTML;
		input.value=temp;
		child=father.appendChild(td);
		
			child.innerHTML=temp;
		}
		td=document.createElement('TD');
		child=father.appendChild(td);
		child.innerHTML="<button onclick=\"Remove(this)\">Rimuovi</button>"
		td=document.createElement('TD');
		td.hidden=true; 
		child=father.appendChild(td);
		input=document.createElement('INPUT');
		input.value=count;
		child.appendChild(input);
		
		object.removeChild(object.children[5]);
		
	}

	function Remove(object)
	{
		if(count!=0)
			count--;
		object=object.parentNode;
		object=object.parentNode;
		var child=object.children[6];
		var position=child.children[0].value;
		var id=object.id;
		father=object.parentNode;
		father.removeChild(object);
		var row=document.getElementById(id);
		var td=document.createElement('TD');
		child=row.appendChild(td);
		child.innerHTML="<button onclick=\"Select(this)\">Seleziona</button>"
		while((child=father.children[position-1])!= null)
		{
			child.children[0].children[0].name="P"+position;
			child.children[6].children[0].value-=1;
			position++;
		}
	}
	</script>
@stop