@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small>lista prodotti valutazione</small></h1>
        		<ol class="breadcrumb">
          			<li class="active"><i class="fa fa-dashboard"></i> Bookshelf/catalogo/lista prodotti valutazione</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Lista prodotti valutazione</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Ricercatore</th>
							<th class="header">Titolo del prodotto</th>
							<th class="header">Azione</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($ricercatori as $ricercatore)
							<tr>
								<td>{{{ $ricercatore->nome }}} {{{ $ricercatore->cognome }}}</td>
								<td>
									<small>		
										@foreach ($ricercatore->prodottiDiRicerca as $prodotto)
											{{{ $prodotto->titolo_del_prodotto_di_ricerca }}} <br>
										@endforeach
									</small>
								<td>
									{{ Form::model($ricercatore, ['route' => 'invio-prodotto-processo-valutazione']) }}
										{{ Form::hidden('id') }}
										{{ Form::submit('Invia alla VQR') }}
									{{ Form::close() }}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
</div>
</div>
</div>
</div>
@stop