@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Valutazione <small>Periodi valutazione</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
          			<li class="active">Periodi valutazione</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Periodi valutazione</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Data Inizio</th>
							<th class="header">Data Fine</th>
						</tr>
					</thead>
					<tbody>
						
							<tr>
							<td>{{$periodo->data_inizio}}</td><td>{{$periodo->data_fine}}</td>
							</tr>
						
						
					</tbody>
				</table>
			</div>
			</div>
			</div>
			</div>
@stop