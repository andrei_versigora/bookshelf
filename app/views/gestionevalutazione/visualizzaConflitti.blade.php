@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Validazione <small>lista conflitti sui prodotti</small></h1>
        		<ol class="breadcrumb">
          			<li class="active"><i class="fa fa-dashboard"></i> Bookshelf/validazione/lista conflitti sui prodotti</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Lista prodotti valutazione</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Titolo del prodotto</th>
							<th class="header">Ricercatore</th>
							<th class="header">Azione</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($prodotti as $prodotto)
							<tr>
								<td>{{{ $prodotto->titolo_del_prodotto_di_ricerca }}}</td>
								<td>{{{ $prodotto->nome }}} {{{ $prodotto->cognome }}}</td>
								<td>
									{{ Form::model($prodotto, ['route' => 'elimina-prodotto-processo-valutazione']) }}
										{{ Form::hidden('id_prodotto_di_ricerca') }}
										{{ Form::hidden('id_lista_prodotti_per_valutazione') }}
										{{ Form::submit('Elimina') }}
									{{ Form::close() }}
								</td>
								<td>
									{{ Form::model($prodotto, ['route' => 'lista-prodotti-da-valutare-ricercatore']) }}
										{{ Form::hidden('id_ricercatore') }}
										{{ Form::hidden('id_lista_prodotti_per_valutazione') }}
										{{ Form::submit('Lista dei prodotti da valutare') }}
									{{ Form::close() }}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
</div>
</div>
</div>
</div>
@stop