@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Valutazione <small>imposta periodo valutazione</small></h1>
        		<ol class="breadcrumb">
        			<li><a href="{{ url('/')}}">Home</a></li>
          			<li class="active">Imposta periodo valutazione</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Lista prodotti valutazione</div>

		<div class="panel-body">
			{{ Form::open([
					'route' => 'gestione-periodo-valutazione',
					'method' => 'post'
					]) }}

			@if ($errors->has('data_inizio'))
		    	<div class="error">{{{ $errors->first('data_inizio') }}}<div>
		    @endif
			{{ Form::text('data_inizio', null, array(
						'placeholder' => 'data inizio',
						'autofocus' => 'true'
						)) }}<br>

			@if ($errors->has('data_fine'))
		    	<div class="error">{{{ $errors->first('data_fine') }}}<div>
		    @endif
			{{ Form::text('data_fine', null, array(
						'placeholder' => 'data fine'
						)) }}<br>

			@if ($errors->has('anno_inizio'))
		    	<div class="error">{{{ $errors->first('anno_inizio') }}}<div>
		    @endif
			{{ Form::text('anno_inizio', null, array(
						'placeholder' => 'anno inizio'
						)) }}<br>
						
			@if ($errors->has('anno_fine'))
		    	<div class="error">{{{ $errors->first('anno_fine') }}}<div>
		    @endif
			{{ Form::text('anno_fine', null, array(
						'placeholder' => 'anno fine'
						)) }}<br>
			
			{{ Form::submit('Imposta periodo')}}
			{{ Form::reset('Annulla')}}
			{{ Form::close() }}
</div>
</div>
</div>
@stop