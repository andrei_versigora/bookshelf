<html>
<head>
	<title>lista Prodotti da validare</title>
</head>
<body>
	<h1>Lista Prodotti</h1>
	Ordina per:
	
	{{Form::open(array('action' => 'gestionevalidazione\ValidationManager@viewProductToBeValidate','method' => 'get'))}}
	{{Form::hidden($name='order', $value='titolo_del_prodotto_di_ricerca')}}
	{{Form::submit($value='Titolo')}}
	{{Form::close()}}
	{{Form::open(array('action' => 'gestionevalidazione\ValidationManager@viewProductToBeValidate','method' => 'get'))}}
	{{Form::hidden($name='order', $value='tipologia_del_prodotto')}}
	{{Form::submit($value='Tipologia')}}
	{{Form::close()}}
	{{Form::open(array('action' => 'gestionevalidazione\ValidationManager@viewProductToBeValidate','method' => 'get'))}}
	{{Form::hidden($name='order', $value='Autore')}}
	{{Form::submit($value='Autore')}}
	{{Form::close()}}

	<br> 
	<table>
	@foreach ($prodotti as $prodotto)
		<tr>
			<td>{{{ $prodotto->titolo_del_prodotto_di_ricerca }}}</td>
			<td>Autori: 
				@foreach ($prodotto->ricercatori as $ricercatore)
					{{{ $ricercatore->cognome }}} {{{ $ricercatore->nome }}}, 
				@endforeach
			
			</td>
			<td>{{{ $prodotto->descrizione }}}</td>
			<?php $link="visualizza=".$prodotto->id ?>
			<td><a href={{{$link}}}>Vai all'id</a></td>
			
		</tr>
	@endforeach
    </table>
    	
</body>
</html>