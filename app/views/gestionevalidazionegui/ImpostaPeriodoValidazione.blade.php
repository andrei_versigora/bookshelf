@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Validazione <small>imposta periodo</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
          			<li class="active">Imposta periodo di validazione</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="col-lg-12">
		<div class="panel panel-primary">
		    <div class="panel-heading"> Inserisci le date</div>
		      	<div class="panel-body">
					{{Form::open(array(
					'action' => 'GestioneValidazione\ValidationManager@setValidationPeriod',
					'method' => 'post'
					))}}

				<div class="error">
					<ul>
					@foreach ($errors->getMessages() as $message)
						<li>{{ $message[0] }}</li>
					@endforeach
					</ul>
				</div>

				<label>Data Inizio immissione prodotti alla validazione</label>  <input type="date" name="data_inizio" value={{\Input::old('data_inizio')}}><br><br>
				<label>Data Fine immissione prodotti alla validazione</label>  <input type="date" name="data_fine" value={{\Input::old('data_fine')}}><br><br>
				<label>Data Inizio invio validazione da parte del Consiglio di Dipartimento</label>  <input type="date" name="data_inizio_consiglio" value={{\Input::old('data_inizio_consiglio')}}><br><br>
				<label>Data Fine validazione da parte del Consiglio di Dipartimento</label>  <input type="date" name="data_fine_consiglio" value={{\Input::old('data_fine_consiglio')}}><br><br>
				<label>Data Inizio validazione da parte del Concilio d'Area</label>  <input type="date" name="data_inizio_concilio" value={{\Input::old('data_inizio_concilio')}}><br><br>
				<label>Data Fine validazione da parte dek Concilio d'Area </label>  <input type="date" name="data_fine_concilio" value={{\Input::old('data_fine_concilio')}}><br><br>
				{{Form::submit()}}
					{{Form::close()}}

				</div>
		</div>
	</div>
</div>

@stop