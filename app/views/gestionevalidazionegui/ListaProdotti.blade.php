@extends('layout/master')

@section('navpath')

<div class="row">
  	<div class="col-lg-12">
		<h1>Validazione <small>scegli i prodotti da validare</small></h1>
    		<ol class="breadcrumb">
      			<li><a href="{{ url('/')}}">Home</a></li>
          		<li class="active">Sottometti alla validazione</li>
    		</ol>
  	</div>
</div>

@stop

@section('content')
	
<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> Lista prodotti valutazione</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Titolo del prodotto</th>
							<th class="header">Descrizione</th>
							<th class="header">Autori</th>
							<th class="header">Tipologia del prodotto</th>
							<th class="header">Azione</th>
						</tr>
					</thead>
					<tbody>
						@if ($errors->has('id'))
    						<div class="error">{{{ $errors->first('id') }}}<div>
   						@endif
						@foreach ($prodotti as $prodotto)
							<tr>
								<td>{{{ $prodotto->titolo_del_prodotto_di_ricerca }}}</td>
								<td>{{{ $prodotto->descrizione }}}</td>
								<td>
									<ul>
									@foreach ($prodotto->ricercatori as $ricercatore)
										<li>{{{ $ricercatore->cognome }}} {{{ $ricercatore->nome }}}</li>
									@endforeach
									</ul>
								</td>
								<td>{{$prodotto->tipologia_del_prodotto}}</td>
								<td>
									 @if(Auth::user()->tipologia==Utente::RICERCATORE){{ Form::model($prodotto, ['route' => ['valida-prodotto', $prodotto->id]]) }}
										{{ Form::submit('Seleziona') }}
									{{ Form::close() }} @endif
               	 					@if(Auth::user()->tipologia==Utente::RES_DIPARTIMENTO || Auth::user()->tipologia==Utente::RES_DISCIPLINARE)
									{{ Form::model($prodotto, ['route' => ['valida-prodotto', $prodotto->id]]) }}
										{{ Form::hidden('id') }}
										{{ Form::submit('Valida') }}
									{{ Form::close() }} <br>
									{{ Form::model($prodotto, ['route' => ['invalida-prodotto', $prodotto->id]]) }}
										{{ Form::hidden('id') }}
										{{ Form::submit('Invalida') }}
									{{ Form::close() }} @endif
								</td>
							</tr>
						@endforeach
					</tbody>
					</table>

</div>
</div>
</div>
</div>
@stop