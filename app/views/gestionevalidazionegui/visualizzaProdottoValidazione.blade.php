<html>
<head>
	<title>Visualizza dettaglio</title>
</head>
<body>
	@if ($errors->has('id'))
    	<div class="error">{{{ $errors->first('id') }}}<div>
	@endif
	<h1>{{{ $prodotto->titolo_del_prodotto_di_ricerca }}}</h1>

	@foreach ($prodotto->ricercatori as $ricercatore)
		<p>{{{ $ricercatore->cognome }}} {{{ $ricercatore->nome }}}</p> 
	@endforeach
	
	<?php $url="invalida=".$prodotto->id ?>
	<a href={{$url}}> Invalida</a>
	<?php $url="valida=".$prodotto->id ?>
	<a href={{$url}}> Valida</a>

</body>
</html>