<!DOCTYPE html>
<html>
<head>
	<title>
		UniSa Bookshelf
	</title>

{{ HTML::style('css/bootstrap.css') }}
{{ HTML::style('css/font-awesome.min.css') }}
{{ HTML::style('css/bootstrap-dialog.css') }}
{{ HTML::style('css/multiple-select.css') }}
{{ HTML::style('css/style.css') }}

</head>
<body>
<div id='wrapper'>

	@include('layout/menu')

	<div id="page-wrapper">

	<!--	@include('layout/navpath') -->

		@section('navpath')
        
        @show

		@section('shortcut')

		@show

		@section('content')

		@show
	</div>
</div>


{{ HTML::script('js/jquery-1.10.2.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}

{{ HTML::script('js/bootstrap-dialog.js') }}
{{ HTML::script('js/jquery.multiple.select.js') }}
{{ HTML::script('js/script.js') }}
</body>
</html>