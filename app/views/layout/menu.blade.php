﻿  <!-- Sidebar -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="{{{ route('home') }}}">Unisa Bookshelf</a> 
    </div>
    
    <!-- menù a sinistra INIZIO -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
        <li ><a href="{{{ route('home') }}}"><i class="fa fa-home"></i> Home </a></li><!-- class="active" -->
    
      @if(Auth::user()->tipologia==Utente::RICERCATORE)    
    	<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Catalogo <b class="caret"></b></a>
              <ul class="dropdown-menu">
              	<li><a href="{{{ route('prodotti') }}}"><i class="fa fa-tasks"></i> I miei Prodotti</a></li>
              </ul>
        </li>
        @endif

        <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Validazione <b class="caret"></b></a>
              <ul class="dropdown-menu">
                @if(Auth::user()->tipologia==Utente::RICERCATORE)<li><a href="{{{ route('lista-prodotti-da-validare') }}}"><i class="fa fa-tasks"></i> Sottometti prodotti alla <br>validazione</a></li>@endif
                @if(Auth::user()->tipologia==Utente::RES_DIPARTIMENTO || Auth::user()->tipologia==Utente::RES_DISCIPLINARE)<li><a href="{{{ route('visualizza-prodotti-da-validare') }}}"><i class="fa fa-tasks"></i> Valida Prodotti</a></li>@endif
                @if (AuthUtils::amministratore())
                  <li><a href="{{{ route('imposta-periodo-validazione') }}}"><i class="fa fa-wrench"></i> Imposta periodo validazione</a></li>
                @endif 
              </ul>
        </li>

        <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Valutazione <b class="caret"></b></a>
              <ul class="dropdown-menu">
                 @if(Auth::user()->tipologia==Utente::RICERCATORE)
                <li><a href="{{{ route('lista-prodotti-da-valutare') }}}"><i class="fa fa-tasks"></i> Inserisci lista valutazione</a></li>
                @endif
                @if(Auth::user()->tipologia==Utente::RICERCATORE || Auth::user()->tipologia==Utente::AMMINISTRATORE)
                <li><a href="{{{ route('periodi-valutazione') }}}"><i class="fa fa-tasks"></i> Periodo valutazione</a></li>
                @endif
                @if(Auth::user()->tipologia==Utente::DIR_DISCIPLINARE || Auth::user()->tipologia==Utente::RES_ATENEO)
                  <li><a href="{{{ route('visualizza-conflitti') }}}"><i class="fa fa-tasks"></i> Visualizza conflitti</a></li>
                @endif
                @if (AuthUtils::amministratore())
                  <li><a href="{{{ route('gestione-periodo-valutazione') }}}"><i class="fa fa-tasks"></i> Imposta periodo valutazione</a></li>
                @endif 

                <li><a href="{{{ route('lista-prodotti') }}}"><i class="fa fa-tasks"></i> Lista prodotti</a></li> 

              </ul>
        </li>
    
        <!--    <li><a href="{{{ route('modifica-prodotto', ['tipologia' => 'articolo_su_rivista']) }}}"><i class="fa fa-tasks"></i> Modifica Prodotto</a></li> -->
            
      <li><a href="{{{ route('ricerca-prodotti') }}}"><i class="fa fa-search"></i> Ricerca Prodotto</a></li>

      @if (AuthUtils::amministratore())
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Utenti <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{{ route('inserisci-utente') }}}"><i class="fa fa-user"></i> Inserisci utente</a></li>
              </ul>
        </li>

        <li><a href="{{{ route('ricerca-utenti') }}}"><i class="fa fa-search"></i> Ricerca Utente</a></li> 

      @endif  

      

      </ul>
      <!-- menù a sinistra FINE --> 
      
      <!-- menù in alto a destra INIZIO -->
      <ul class="nav navbar-nav navbar-right navbar-user">
      	

      @if(Auth::user()->tipologia==Utente::RICERCATORE)  
      <li class="dropdown user-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><font color="#428bca"><i class="fa fa-tasks"></i> Inserisci prodotto<b class="caret"></b></font></a></font>
          <ul class="dropdown-menu user-dropdown">
            @foreach (GuiUtils::productTipologies(false) as $id => $name)
              <li><a href="{{{ route('form-inserisci-prodotto', ['tipologia' => $id]) }}}"><i class="fa fa-tasks"></i> {{{ $name }}}</a></li>
            @endforeach
          </ul>
        </li>
      @endif

       <li class="dropdown user-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{{ Auth::user()->nome }}} {{{ Auth::user()->cognome }}}<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{{ route('form-modifica-utente-me') }}}"><i class="fa fa-gear"></i> Modifica dati personali</a></li>
            <li class="divider"></li>
            <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> LogOut</a></li>
          </ul>
        </li>
      </ul> 
      <!-- menù in alto a destra FINE --> 
    </div>
    <!-- /.navbar-collapse --> 
  </nav>