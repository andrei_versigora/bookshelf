@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Operazione completata! <small>Complimenti!</small></h1>
      	</div>
</div>

@stop

@section('content')
	
	<p>
		@if (!empty($message))
			{{ $message }}
		@else
			L'operazione è stata completata con successo!
		@endif
	</p>
	<div>
		<a href="{{ URL::previous() }}" type="button" class="btn btn-default">Torna indietro</a>
	</div>

@stop