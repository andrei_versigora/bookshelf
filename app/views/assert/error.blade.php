@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Ops! Si &egrave; verificato un errore! <small>Spiacente!</small></h1>
      	</div>
</div>

@stop

@section('content')
	
	<p>
		@if (!empty($error))
			{{ $error }}
		@else
			Operazione completata con successo!
		@endif
	</p>

	<div>
		<a href="{{ URL::previous() }}" type="button" class="btn btn-default">Torna indietro</a>
	</div>


@stop