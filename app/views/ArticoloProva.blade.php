<html>
<head>
	<title>Inserimento prova</title>
</head>
<body>
{{ Form::open(array('action' => 'ArticoloSuRivista@InserisciProdotto', 'method' => 'Post')) }}

Titolo del Prodotto di Ricerca: {{ Form::text('TitoloDelProdottoDiRicerca')}}<br>
Descrizione: {{ Form::text('Descrizione')}}<br>
DataPubblicazione: <input type="date" name="DataPubblicazione"> <br>
Tipologia del Prodotto: {{ Form::select('TipologiaDelProdotto',
							array('EdizioneCriticaTraduzioneCommento' => 'EdizioneCriticaTraduzioneCommento',
								  'AttoDiCongresso' => 'AttoDiCongresso',
								  'Altro' => 'Altro',
								  'LibroCapitolo' => 'LibroCapitolo',
								  'ArticoloSuRivista' => 'ArticoloSuRivista'))}}<br>
<div id="ArticoloSuRivista">DOI: {{Form::text('DOI')}}<br>
TitoloRivista: {{Form::text('TitoloRivista')}}<br>
ISSN: {{Form::text('ISSN')}}<br>
NumPaginaInizio: {{Form::text('NumPaginaInizio')}}<br>
NumPaginaFine: {{Form::text('NumPaginaFine')}}<br>
NumRivista: {{Form::text('NumRivista')}}<br>
{{Form::submit('Cliccami')}}

{{Form::close()}}
</div>
</body>
</html>