<html>
<head>
	<title>Inserimento prova</title>
</head>
<body>
{{ Form::open(array('action' => 'ArticoloSuRivista@Modifica', 'method' => 'Post')) }}
<?php
$prodotto=ProdottoDiRicerca::find(3);
$articolo=ModelArticoloSuRivista::where('IDProdottoDiRicerca',3)->get();
$articolo=$articolo[0];
?>
Titolo del Prodotto di Ricerca: {{ Form::text('TitoloDelProdottoDiRicerca',$value=$prodotto->TitoloDelProdottoDiRicerca)}}<br>
Descrizione: {{ Form::text('Descrizione',$value=$prodotto->Descrizione)}}<br>
DataPubblicazione: <input type="date" name="DataPubblicazione" value= <?php echo "$prodotto->DataPubblicazione"; ?> > <br>
Tipologia del Prodotto: {{ Form::select('TipologiaDelProdotto',
							array('EdizioneCriticaTraduzioneCommento' => 'EdizioneCriticaTraduzioneCommento',
								  'AttoDiCongresso' => 'AttoDiCongresso',
								  'Altro' => 'Altro',
								  'LibroCapitolo' => 'LibroCapitolo',
								  'ArticoloSuRivista' => 'ArticoloSuRivista'),$prodotto->TipologiaDelProdotto)}}<br>
<div id="ArticoloSuRivista">DOI: {{Form::text('DOI',$value=$articolo->DOI)}}<br>
TitoloRivista: {{Form::text('TitoloRivista',$value=$articolo->TitoloRivista)}}<br>
ISSN: {{Form::text('ISSN',$value=$articolo->ISSN)}}<br>
NumPaginaInizio: {{Form::text('NumPaginaInizio',$value=$articolo->NumPaginaInizio)}}<br>
NumPaginaFine: {{Form::text('NumPaginaFine',$value=$articolo->NumPaginaFine)}}<br>
NumRivista: {{Form::text('NumRivista',$value=$articolo->NumRivista)}}<br>
{{Form::submit('Cliccami')}}

{{Form::close()}}
</div>
</body>
</html>