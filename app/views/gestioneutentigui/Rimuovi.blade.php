<!DOCTYPE html>
<html>
<head>
	<title>Seleziona utente da rimuovere</title>
</head>
<body>
    {{ Form::open(['route' => 'form-rimuovi', 'method' => 'post']) }}
<h1>Seleziona l'utente da rimuovere:</h1><br>

@foreach (Utente::orderBy('username')->get() as $user)
<a href="{{route('form-rimuovi-utente', ['id' => $user->id])}}">{{$user->username}} {{$user->nome}} {{$user->cognome}}</a><br>
@endforeach

	{{ Form::close() }}
</body>
</html>