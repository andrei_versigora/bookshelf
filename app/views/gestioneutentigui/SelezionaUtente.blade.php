<!DOCTYPE html>
<html>
<head>
	<title>Seleziona utente da modificare</title>
</head>
<body>
	{{ Form::open(['route' => 'form-seleziona-utente', 'method' => 'post']) }}
<h1>Clicca sull'username dell'utente da modificare:</h1><br>

@foreach (Utente::orderBy('username')->get() as $user)
<a href=" {{route('form-modifica-utente', ['id' => $user->id])}}">{{$user->username}} {{$user->nome}} {{$user->cognome}}</a><br>
@endforeach

	{{ Form::close() }}
</body>
</html>