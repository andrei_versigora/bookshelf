<!DOCTYPE html>
<html lang="en">
<head>
<title>Login - Unisa Bookshelf</title>

{{ HTML::style('css/bootstrap.css') }}
{{ HTML::style('css/style.css') }}
{{ HTML::style('css/login.css') }}
{{ HTML::style('font-awesome/css/font-awesome.css') }}
{{ HTML::style('http://cdn.oesmith.co.uk/morris-0.4.3.min.css') }}

</head>

<body>
<div class="container">
  <div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
    <!--  <h1 class="text-center login-title">Unisa Bookshelf</h1> -->
      <div class="account-wall"> <img class="profile-img" src="{{ asset('images/log.png') }}" alt="Logo">
        {{ Form::open(array(
        'action' => 'GestioneUtenti\UserManagementController@login',
        'method' => 'post',
        'class' => 'form-signin'
        )) }}

        @if ($errors->has('nomeUtente'))
          <div class="error">{{{ $errors->first('nomeUtente') }}}<div>
        @endif
        
          {{ Form::text('nomeUtente', null, array(
                'placeholder' => 'Nome utente',
                'autofocus' => 'true',
                'class' => 'form-control',
                'type' => 'text'
                )) }}

          @if ($errors->has('password'))
          <div class="error">{{{ $errors->first('password') }}}<div>
        @endif


          {{ Form::password('password', array(
                'placeholder' => 'Password',
                'class' => 'form-control'
                )) }}

          {{ Form::button('Login', [
                'type' => 'submit',
                'class' => 'btn btn-lg btn-primary btn-block'
              ]) }}
        
        {{ Form::close() }}
      </div>
      </div>
  </div>
</div>

{{ HTML::script('js/jquery-1.10.2.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/script.js') }}

{{ HTML::script('//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') }}
{{ HTML::script('http://cdn.oesmith.co.uk/morris-0.4.3.min.js') }}
{{ HTML::script('js/morris/chart-data-morris.js') }}

{{ HTML::script('js/tablesorter/jquery.tablesorter.js') }}
{{ HTML::script('js/tablesorter/tables.js') }}

</body>
</html>
