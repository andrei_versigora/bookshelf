@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Amministrazione <small>ricerca utente</small></h1>
        		<ol class="breadcrumb">
          			<li class="active"><i class="fa fa-dashboard"></i> Bookshelf/amministrazione/ricerca utente</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

<div class="col-lg-12">
	        <div class="panel panel-primary">
	          <div class="panel-heading"> Dati dell'utente {{ $utente->username }} </div>
	          	<div class="panel-body">

	{{ Form::model($utente, ['route' => 'rimuovi-utente', 'method' => 'post']) }}

		<!-- Campi dell' utente -->
        <table>
		{{ Form::hidden('id') }}
		<tr><td>Nome:<td>{{ $utente->nome }}</tr>
		<tr><td>Cognome:<td>{{ $utente->cognome }}</tr>
		<tr><td><input type="submit" value="Rimuovi"><td><a href="{{{ route('ricerca-utenti') }}}"><button type="button" class="btn btn-default">Annulla</button></a></tr>
</table>
{{ Form::close();
   $message = Session::get('message');
   echo $message;
}}

</div>
</div>
</div>
</div>


@stop