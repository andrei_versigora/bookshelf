@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Utenti <small>inserisci nuovo utente</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
          			<li>{{ link_to_route('ricerca-utenti', 'Utenti') }}</li>
          			<li class="active">Inserisci utente</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

<div class="col-lg-12">
	        <div class="panel panel-primary">
	          <div class="panel-heading"> Inserisci i dati dell'utente</div>
	          	<div class="panel-body">


	{{ Form::open(['route' => 'inserisci-utente', 'method' => 'post']) }}
	<div class="error">
					<ul>
					@foreach ($errors->getMessages() as $message)
						<li>{{ $message[0] }}</li>
					@endforeach
					</ul>
				</div>



		<!-- Campi di un utente -->
		
		<table>
		<tr><td><br>Username <td>  <br><input type="text" name="txt_username"></tr>
		<tr><td><br>Password<td><br><input type="text" name="txt_password"></tr>
		<tr><td><br>Nome<td><br><input type="text" name="txt_nome"></tr>
		<tr><td><br>Cognome<td><br><input type="text" name="txt_cognome"></tr>
		<tr><td><br>Email<td><br><input type="text" name="txt_email"></tr>
		<tr><td><br>Telefono<td><br><input type="text" name="txt_telefono"></tr>
		<tr><td><br>Cellulare<td><br><input type="text" name="txt_cellulare"></tr>
		<tr><td><br>Via<td><br><input type="text" name="txt_via"></tr>
		<tr><td><br>Numero Civico<td><br><input type="text" name="txt_numcivico"></tr>
		<tr><td><br>Città<td><br><input type="text" name="txt_citta"></tr>
		<tr><td><br>Provincia<td><br><input type="text" name="txt_provincia"></tr>
		<tr><td><br>Data di servizio<td><br><input type="text" name="txt_dataservizio"></tr>
		<tr><td><br>Tipologia<td><br>{{
			Form::select('txt_tipologia', [
			        'NULL' => '',
					'ricercatore' => 'Ricercatore',
					'amministratore' => 'Amministratore',
					'resp.dipartimento' => 'Responsabile Dipartimento',
					'resp.disciplinare' => 'Responsabile Area Disciplinare',
				    'dir.disciplinare' => 'Direttore Area Disciplinare',
				    'res.ateneo' => 'Responsabile Ateneo',
				], Input::get('txt_tipologia'));
		}}</tr>
		<tr><td><br>Area Scientifica<td><br>{{
			Form::select('txt_areascientifica', [
			        'NULL' => '', 
					'1' => 'Scienze matematiche e informatiche',
					'2' => 'Scienze fisiche',
					'3' => 'Scienze chimiche',
					'4' => 'Scienze della terra',
					'5' => 'Scienze biologiche',
					'6' => 'Scienze mediche',
					'7' => 'Scienze agrarie e veterinarie',
					'8' => 'Scienze civile ed architettura',
					'9' => 'Scienze industriale e dell\'informazione',
					'10' => 'Scienze dell\'antichità, filologico-letterarie e storico artistiche',
					'11' => 'Scienze storiche, filosofiche, psicologiche e pedagogiche',
					'12' => 'Scienze giuridiche',
					'13' => 'Scienze politiche e sociali',
					'14' => 'Scienze economiche e statistiche',
				], Input::get('txt_areascientifica'));
		}}</tr>
			<tr><td><br><br><input type="submit" value="Inserisci"><td><br><br><input type="reset" value="Annulla"></tr>
</table>
	
	{{ Form::close();
	 $message = Session::get('message');
	 echo $message; }}

</div>
</div>
</div>
</div>
@stop