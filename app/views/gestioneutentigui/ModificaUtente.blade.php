@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Amministrazione <small>ricerca utente</small></h1>
        		<ol class="breadcrumb">
          			<li class="active"><i class="fa fa-dashboard"></i> Bookshelf/amministrazione/ricerca utente</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

<div class="col-lg-12">
	        <div class="panel panel-primary">
	          <div class="panel-heading"> Modifica dati dell'utente {{$utente->username}}</div>
	          	<div class="panel-body">

	{{ Form::model($utente, ['route' => 'modifica-utente', 'method' => 'post']) }}
		<div class="error">
					<ul>
					@foreach ($errors->getMessages() as $message)
						<li>{{ $message[0] }}</li>
					@endforeach
					</ul>
				</div>


		<!-- Campi di un utente -->
        <table>
		{{ Form::hidden('id') }}
		<tr><td>Nome:<td>{{ Form::text('nome') }}</tr>
		<tr><td>Cognome:<td>{{ Form::text('cognome') }}</tr>
		<br>
		<br>
		<tr><td>Password:<td>{{ Form::password('password', '') }}</tr>
		<tr><td>Ripeti password:<td>{{ Form::password('password_confirmation') }}</tr>
		<br>
		<br>
		<tr><td>Email:<td>{{ Form::text('email') }}</tr>
		<tr><td>Telefono:<td>{{ Form::text('telefono') }}</tr>
		<tr><td>Cellulare:<td>{{ Form::text('cellulare') }}</tr>
		<tr><td>Via:<td>{{ Form::text('via') }}</tr>
		<tr><td>Numero Civico:<td>{{ Form::text('num_civico') }}</tr>
		<tr><td>Città:<td>{{ Form::text('città') }}</tr>
		<tr><td>Provincia:<td>{{ Form::text('provincia') }}</tr>
		<tr><td>Data di servizio:<td>{{ Form::text('data_servizio') }}</tr>
		<tr><td>Tipologia:<td>{{
			Form::select('tipologia', [
			        'NULL' => '',
					'ricercatore' => 'Ricercatore',
					'amministratore' => 'Amministratore',
					'resp.dipartimento' => 'Responsabile Dipartimento',
					'resp.disciplinare' => 'Responsabile Area Disciplinare',
				    'dir.disciplinare' => 'Direttore Area Disciplinare',
				    'res.ateneo' => 'Responsabile Ateneo',
				]);
		}}</tr>
		<tr><td>Area Scientifica:<td>{{
			Form::select('id_area_scientifica', [
			        'NULL' => '',
					'1' => 'Scienze matematiche e informatiche',
					'2' => 'Scienze fisiche',
					'3' => 'Scienze chimiche',
					'4' => 'Scienze della terra',
					'5' => 'Scienze biologiche',
					'6' => 'Scienze mediche',
					'7' => 'Scienze agrarie e veterinarie',
					'8' => 'Scienze civile ed architettura',
					'9' => 'Scienze industriale e dell\'informazione',
					'10' => 'Scienze dell\'antichità, filologico-letterarie e storico artistiche',
					'11' => 'Scienze storiche, filosofiche, psicologiche e pedagogiche',
					'12' => 'Scienze giuridiche',
					'13' => 'Scienze politiche e sociali',
					'14' => 'Scienze economiche e statistiche',
				]);
		}}</tr>
		<tr><td><input type="submit" value="Modifica"><td><a href="{{route('ricerca-utenti')}}"><button type="button" class="btn btn-default">Annulla</button></a></tr>
</table>
{{ Form::close();
   $message = Session::get('message');
   echo $message;
}}

</div>
</div>
</div>
</div>
@stop