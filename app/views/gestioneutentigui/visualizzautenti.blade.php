<!DOCTYPE html>
<html>
<head>
    <title>Pagina iniziale</title>
</head>
<body>
    <h1>UniSa Bookshelf</h1>

    <ul>
    	@foreach ($utenti as $element)
    		<li>{{ $element->username }} - {{ $element->nome }} - {{ $element->cognome }} - {{ $element->tipologia }}</li>
    	@endforeach
    </ul>

</body>
</html>
