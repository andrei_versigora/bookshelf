@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Bookshelf <small>home</small></h1>
        		<ol class="breadcrumb">
          			<li class="active"><i class="fa fa-home"></i> home</li>
        		</ol>
      	</div>
</div>

@stop

@section('shortcut')

<div class='row'>

  <div class="col-lg-3">
        <div class="panel panel-info"> <!-- questo fa cambiare colore -->
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6"> 
                <i class="fa fa-tasks fa-5x"></i> 
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">{{ Auth::user()->prodotti()->count() }}</p>
                <p class="announcement-text">Prodotti</p>
              </div>
            </div>
          </div>
          <a href="{{ route('prodotti') }}">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6"> Vedi Prodotti </div>
              <div class="col-xs-6 text-right"> <i class="fa fa-arrow-circle-right"></i> </div>
            </div>
          </div>
          </a> 
        </div>
  </div>

  <div class="col-lg-3">
    <div class="panel panel-warning">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6"> <i class="fa fa-tasks fa-5x"></i> </div>
          <div class="col-xs-6 text-right">
            <p class="announcement-heading">&infin;</p>
            <p class="announcement-text">In Validazione</p>
          </div>
        </div>
      </div>
      <a href="#">
        <div class="panel-footer announcement-bottom">
          <div class="row">
            <div class="col-xs-6"> Vedi Prodotti </div>
            <div class="col-xs-6 text-right"> <i class="fa fa-arrow-circle-right"></i> </div>
          </div>
        </div>
      </a> 
    </div>
  </div>

      <div class="col-lg-3">
        <div class="panel panel-warning">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6"> <i class="fa fa-tasks fa-5x"></i> </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">&infin;</p>
                <p class="announcement-text">In Valutazione</p>
              </div>
            </div>
          </div>
          <a href="#">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6"> Vedi Prodotti </div>
                <div class="col-xs-6 text-right"> <i class="fa fa-arrow-circle-right"></i> </div>
              </div>
            </div>
          </a> 
        </div>
      </div>

      <div class="col-lg-3">
        <div class="panel panel-success">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6"> <i class="fa fa-check fa-5x"></i> </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">&infin;</p>
                <p class="announcement-text">Validati/Valutati</p>
              </div>
            </div>
          </div>
          <a href="#">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6"> Vedi Prodotti </div>
              <div class="col-xs-6 text-right"> <i class="fa fa-arrow-circle-right"></i> </div>
            </div>
          </div>
          </a> 
        </div>
      </div>
</div>

@stop

@section('content')
<!-- Per adesso questo codice resta commentato
	<div class="row">
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-clock-o"></i> Date Utili</h3>
          </div>
          <div class="panel-body">
            <div id="morris-chart-donut">
            <table class="table table-bordered table-hover table-striped tablesorter">
                <tbody>
                  <tr>
                    <td>10/21/2013</td>
                    <td>termine ultimo validazione 2012</td>
                  </tr>
                  <tr>
                    <td>10/21/2013</td>
                    <td>termine ultimo validazione 2013</td>
                  </tr>
                  <tr>
                    <td>10/21/2013</td>
                    <td>termine ultimo valutazione</td>
                  </tr>
                </tbody>
              </table></div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-clock-o"></i> Attività recenti</h3>
          </div>
          <div class="panel-body">
            <div class="list-group"> <a href="#" class="list-group-item"> <span class="badge">just now</span> <i class="fa fa-calendar"></i> Settata data consegna prodotti </a> <a href="#" class="list-group-item"> <span class="badge">2 hours ago</span> <i class="fa fa-check"></i> Prodotto valutato </a> <a href="#" class="list-group-item"> <span class="badge">2 minutes ago</span> <i class="fa fa-tasks"></i> Prodotto inserito </a> <a href="#" class="list-group-item"> <span class="badge">two days ago</span> <i class="fa fa-check"></i> Prodotto validato </a> </div>
            <div class="text-right"> <a href="#">Vedi tutto <i class="fa fa-arrow-circle-right"></i></a> </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-tasks"></i> Ultimi prodotti visualizzati</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-bordered table-hover table-striped tablesorter">
                <thead>
                  <tr>
                    <th>ID <i class="fa fa-sort"></i></th>
                    <th>Data Creazione <i class="fa fa-sort"></i></th>
                    <th>Titolo <i class="fa fa-sort"></i></th>
                    <th>Autore <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>3326</td>
                    <td>10/21/2013</td>
                    <td>titolo 1</td>
                    <td>ferrucci</td>
                  </tr>
                  <tr>
                    <td>3325</td>
                    <td>10/21/2013</td>
                    <td>titolo 2</td>
                    <td>de lucia</td>
                  </tr>
                  <tr>
                    <td>3324</td>
                    <td>10/21/2013</td>
                    <td>titolo</td>
                    <td>pippo</td>
                  </tr>
                  <tr>
                    <td>3323</td>
                    <td>10/21/2013</td>
                    <td>titolo 5</td>
                    <td>pluto</td>
                  </tr>
                  <tr>
                    <td>3322</td>
                    <td>10/21/2013</td>
                    <td>titolone</td>
                    <td>paperino</td>
                  </tr>
                  <tr>
                    <td>3321</td>
                    <td>10/21/2013</td>
                    <td>storia</td>
                    <td>pdor figlio di kmer</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="text-right"> <a href="#">Vedi tutto<i class="fa fa-arrow-circle-right"></i></a> </div>
          </div>
        </div>
      </div>
    </div>
-->
@stop