@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small>i miei prodotti</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
        			<li>{{ link_to_route('ricerca-prodotti', 'Catalogo') }}</li>
          			<li class="active">I miei prodotti</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> I miei prodotti</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">
				<!--	<caption><h3>Lista dei prodotti</h3></caption> -->
					<thead>
						<tr>
							<th class="header">Titolo del prodotto</th>
							<th class="header">Autori</th>
							<th class="header">Stato Validazione</th>
							<th class="header"></th>
						</tr>
					</thead>
					<tbody>
						@if ($errors->has('id'))
    						<div class="error">{{{ $errors->first('id') }}}<div>
   						@endif
						@foreach ($prodotti as $prodotto)
							<tr>
							<td>
								<a href="{{ route('visualizza-prodotto', ['id' => $prodotto->id]) }}">
									{{{$prodotto->titolo_del_prodotto_di_ricerca}}}
								</a>
							</td>
							<td>
								<small>		
								@foreach ($prodotto->ricercatori as $ricercatore)
									{{{ $ricercatore->cognome }}} {{{ $ricercatore->nome }}},  
								@endforeach
								</small>
							</td>
							<td>{{{$prodotto->stato_validazione}}}</td>
			                <td>
			                <ul>
			                	@if (ResearchProduct::verificaPermessi(Auth::user()->id, $prodotto->id))
				                	<li>
				                		<a class="delete" href="{{ route('elimina-prodotto', ['id' => $prodotto->id]) }}">  Elimina</a>
				                	</li>
				                	<li>
				               			<a href="{{ route('modifica-prodotto', ['id' => $prodotto->id]) }}">  Modifica</a>
				                	</li>
			                	@endif
			                </ul>
			                </td>
							</tr>
						@endforeach
					</tbody>
				</table>

			</div>

		</div>

	</div>

</div>


@stop