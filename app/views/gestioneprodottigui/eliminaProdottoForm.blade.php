<html>
<head>
	<title>Modifica di un Prodotto</title>
</head>
<body>
	<script type="text/javascript">
		function tipologia(valore){
				document.getElementById("ip").value=valore;
			}
	</script>

	<h1>UnisaBookshelf</h1>
	<fieldset>
		<legend>Elimina</legend>
		
			<label style="vertical-align:top">Prodotti:
			<select name="prodotto">
				@foreach ($prodotti as $element)
					
					<option value={{$element->ID}} onClick="tipologia(this.value)">{{$element->TitoloDelProdottoDiRicerca}}</option> 
				@endforeach
			</select>
				{{ Form::open(array('action' => 'CostrLibroCapitolo@deleteProduct', 'method' => 'post')) }}
					<input id="ip" name="idProdotto" type="text" >
				<br>
			<input type="submit" value="Elimina">
			<input type="reset" value="Annulla">
		{{Form::close()}}
	</fieldset>
</body>
</html>