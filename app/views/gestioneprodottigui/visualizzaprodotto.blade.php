@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small>i miei prodotti</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
        			<li>{{ link_to_route('ricerca-prodotti', 'Catalogo') }}</li>
        			<li>{{ link_to_route('prodotti', 'I miei prodotti') }}</li>
          			<li class="active">Dettagli prodotto</li>
        		</ol>
      	</div>
</div>

@stop

@section('content') <ul>
		<li>Titolo del prodotto di ricerca: {{{ $prodotto->titolo_del_prodotto_di_ricerca }}}</li>	
		<li>Descrizione: {{{ $prodotto->descrizione }}}</li>

		@if (is_object($prodotto->dettagli))

			@if ($prodotto->titolo_del_prodotto_di_ricerca == "Altro")
				@include('gestioneprodottigui/dettaglialtro')
			@endif

			@if ($prodotto->titolo_del_prodotto_di_ricerca == "AttoDiCongresso")
				@include('gestioneprodottigui/dettaglicongresso')
			@endif

		@endif
	</ul>

	<br><br>
	<!-- Lo implementiamo così! -->
	<a href="{{ URL::previous() }}" type="button" class="btn btn-default">Torna indietro</a>

 @stop