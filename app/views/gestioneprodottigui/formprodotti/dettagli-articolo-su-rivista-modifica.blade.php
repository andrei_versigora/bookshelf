@extends('gestioneprodottigui/formprodotti/formmodificagenerale')

@section('nav')
	Articolo su rivista
@stop

@section('dettagli')
	<label>Rivista</label> {{Form::text('titolo_rivista',$prodotto->dettagli['titolo_rivista'])}}<br><br>
	<label> DOI </label> {{Form::text('doi',$prodotto->dettagli['doi'], [ 'placeholder' => '10.1234/esempio' ])}}<br><br>
	<label> ISSN </label> {{Form::text('issn',$prodotto->dettagli['issn'])}}<br><br>
	<label>Pagina iniziale </label> {{Form::text('num_pagina_inizio',$prodotto->dettagli['num_pagina_inizio'])}}<br><br>
	<label>Pagina finale </label> {{Form::text('num_pagina_fine',$prodotto->dettagli['num_pagina_fine'])}}<br><br>
	<label> Numero della rivista </label> {{Form::text('num_rivista',$prodotto->dettagli['num_rivista'])}}<br><br>
@stop

