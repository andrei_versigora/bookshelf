@extends('gestioneprodottigui/formprodotti/formmodificagenerale')
@section('nav')
	Libro
        Capitolo di libro
@stop
@section('dettagli')
    <label>Titolo capitolo</label> {{Form::text('titolo_capitolo',$prodotto->dettagli['titolo_capitolo'])}}<br><br>
    <label>Titolo libro</label> {{Form::text('titolo_libro',$prodotto->dettagli['titolo_libro'])}}<br><br>
    <label> DOI </label> {{Form::text('doi',$prodotto->dettagli['doi'])}}<br><br>
    <label>ISBN </label>{{Form::text('isbn',$prodotto->dettagli['isbn'])}}<br><br>
    <label>Pagina iniziale </label> {{Form::text('num_pagina_inizio',$prodotto->dettagli['num_pagina_inizio'])}}<br><br>
    <label>Pagina finale </label> {{Form::text('num_pagina_fine',$prodotto->dettagli['num_pagina_fine'])}}<br><br>
    <label>Casa editrice </label> {{Form::text('casa_editrice',$prodotto->dettagli['casa_editrice'])}}<br><br>
    <label>Data stesura </label>{{Form::text('data_stesura',$prodotto->dettagli['data_stesura'])}}<br><br>
    <label> Editori </label> {{Form::text('nome_editori',$prodotto->dettagli['nome_editori'])}}<br><br>
@stop