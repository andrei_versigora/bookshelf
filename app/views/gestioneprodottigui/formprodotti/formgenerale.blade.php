@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small>Aggiungi un prodotto</small></h1>
        		<ol class="breadcrumb">
        			<li><a href="{{ url('/')}}">Home</a></li>
        			<li>{{ link_to_route('ricerca-prodotti', 'Catalogo') }}</li>
          			<li class="active">
          				Inserisci &quot;
          				@section('nav')
				        @show
				        &quot;
          			</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> I miei prodotti</div>

		<div class="panel-body">

	{{ Form::open(['route' => 'inserisci-prodotto', 'method' => 'post', 'class' => 'prodotto-form']) }}

		
		{{-- Questo è un esempio su come stampare un errore --}}
		<div class="error">
			<ul>
			@foreach ($errors->getMessages() as $message)
				<li>{{ $message[0] }}</li>
			@endforeach
			</ul>
		</div>

		<!-- Campi comuni per tutti i prodotti -->
		{{ Form::label('titolo_del_prodotto_di_ricerca', 'Titolo') }}
		{{ Form::text('titolo_del_prodotto_di_ricerca') }}<br><br>
		
		{{ Form::label('descrizione', 'Descrizione') }}
		{{ Form::textarea('descrizione') }}<br><br>
		{{ Form:: label ('data_pubblicazione', 'Data pubblicazione') }}
		{{ Form:: text('data_pubblicazione','', [ 'placeholder' => 'gg/mm/aaaa' ])}}<br><br>
		{{ Form::label('autori', 'Autori') }}
		<select id="seleziona-autori" name="autori[]" multiple size="4">
			@foreach (Utente::all() as $ricercatore)
				<option value="{{$ricercatore->id}}">{{$ricercatore->nome}} {{$ricercatore->cognome}}</option>
			@endforeach
		</select>
		

		<br><br>
		{{ Form::label('tipologia', 'Tipologia') }}
		{{
			Form::select(null, GuiUtils::productTipologies(false), \Input::get('tipologia'), ['disabled' => 'true']);
		}}
		{{
			Form::hidden('tipologia', \Input::get('tipologia'));
		}}

        <br><br>

		@section('dettagli')
        @show

        <br><br>
		<button type="submit" class="btn btn-default" value="Inserisci">Inserisci</button>
		<button type="reset" class="btn btn-default" value="Annulla">Annulla</button>

	{{ Form::close() }}

</div>
</div>
</div>
 @stop