@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small>modifica prodotto</small></h1>
        		<ol class="breadcrumb">
        			<li><a href="{{ url('/')}}">Home</a></li>
        			<li>{{ link_to_route('ricerca-prodotti', 'Catalogo') }}</li>
          			<li class="active">
          				Modifica &quot;
          				@section('nav')
				        @show
				        &quot;
          			</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

<div class="col-lg-12">
	        <div class="panel panel-primary">
	          <div class="panel-heading"> Modifica i dati del prodotto</div>
	          	<div class="panel-body">


	{{ Form::model($prodotto, ['route' => 'modifica-prodotto', 'method' => 'post', 'class' => 'prodotto-form']) }}

		<div class="error">
			<ul>
			@foreach ($errors->getMessages() as $message)
				<li>{{ $message[0] }}</li>
			@endforeach
			</ul>
		</div>

		{{ Form::hidden('id') }}

		<!-- Campi comuni per tutti i prodotti -->
		Titolo {{ Form::text('titolo_del_prodotto_di_ricerca') }}<br><br>
		Descrizione{{ Form::textarea('descrizione') }}<br><br>
		Data pubblicazione{{ Form::text('data_pubblicazione', date("d/m/Y", strtotime($prodotto->data_pubblicazione)),[ 'placeholder' => 'gg/mm/aaaa' ]) }}<br><br>
		<label style="vertical-align:top">Autori:</label>
		<select id="seleziona-autori" name="autori[]" multiple size="4">
			@foreach (Utente::all() as $ricercatore)


					@if (!empty($prodotto->ricercatori()->find($ricercatore->id)))
					<option value="{{$ricercatore->id}}" selected="true">{{$ricercatore->nome}} {{$ricercatore->cognome}}</option>
					@else
					<option value="{{$ricercatore->id}}">{{$ricercatore->nome}} {{$ricercatore->cognome}}</option>
					@endif


			@endforeach
		</select><br><br>
		Tipologia

		{{
			Form::select(null, GuiUtils::productTipologies(false), $prodotto->tipologia_del_prodotto, ['disabled' => 'true']);
		}}
		{{
			Form::hidden('tipologia', $prodotto->tipologia_del_prodotto);
		}}<br><br>

		

		@section('dettagli')
        @show
      	@section('permessi')
        @show

        <br><br>
		<button type="submit" class="btn btn-default" value="Modifica">Modifica</button>
		<button type="reset" class="btn btn-default" value="Annulla modifiche">Annulla Modifiche</button> 
		<a href="{{{ route('prodotti') }}}"><button type="button" class="btn btn-default">Torna alla lista</button> </a>

	{{ Form::close() }}
</div>
</div>
</div>
</div>
@stop