@extends('gestioneprodottigui/formprodotti/formmodificagenerale')
@section('nav')
	Atti di congresso
@stop
@section('dettagli')
<label>DOI</label> {{Form::text('doi',$prodotto->dettagli['doi'])}}<br><br>
<label>ISBN </label>{{Form::text('isbn',$prodotto->dettagli['isbn'])}}<br><br>
<label>Pagina iniziale </label>{{Form::text('num_pagina_inizio',$prodotto->dettagli['num_pagina_inizio'])}}<br><br>
<label>Pagina finale</label> {{Form::text('num_pagina_fine',$prodotto->dettagli['num_pagina_fine'])}}<br><br>
<label>Convegno</label> {{Form::text('nome_convegno',$prodotto->dettagli['nome_convegno'])}}<br><br>
<label>Editori</label> {{Form::text('editore_convegno',$prodotto->dettagli['editore_convegno'])}}<br><br>
<label>Luogo </label>{{Form::text('luogo_convegno',$prodotto->dettagli['luogo_convegno'])}}<br><br>
@stop