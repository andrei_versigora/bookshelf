@extends('gestioneprodottigui/formprodotti/formgenerale')
@section('nav')
	Commenti specifici
        Traduzioni
        Edizioni Critiche
@stop

@section('dettagli')
	<label>Lingua</label> {{Form::text('lingua')}}<br><br>
	<label>Codici per la trasmissione del testo</label> {{Form::text('codici')}}<br><br>
@stop