@extends('gestioneprodottigui/formprodotti/formmodificagenerale')
@section('nav')
	Altro
@stop
@section('dettagli')
	<label> Sottotipologia </label> {{ Form::select('tipologia_altro',
		['composizione' => 'Composizione',
		  'design' => 'Design',
		  'disegno' => 'Disegno',
		  'performance' => 'Performance',
		  'esposizione' => 'Esposizione',
		  'banca dati' => 'Banca Dati',
		  'software' => 'Software',
		  'brevetto' => 'Brevetto']
		  , $prodotto->dettagli['tipologia_altro'])}}<br><br>
@stop