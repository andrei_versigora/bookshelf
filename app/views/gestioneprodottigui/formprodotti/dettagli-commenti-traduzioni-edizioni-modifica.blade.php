@extends('gestioneprodottigui/formprodotti/formmodificagenerale')
@section('nav')
	Commenti specifici
        Traduzioni
        Edizioni critiche
@stop
@section('dettagli')
	<label>Lingua</label> {{Form::text('lingua', $prodotto->dettagli['lingua'])}}<br><br>
	<label>Codici per la trasmissione del testo</label> {{Form::text('codici', $prodotto->dettagli['codici'])}}<br><br>
	@stop