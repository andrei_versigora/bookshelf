@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small>ricerca prodotti</small></h1>
        		<ol class="breadcrumb">
        			<li><a href="{{ url('/')}}">Home</a></li>
        			<li class="active">{{ link_to_route('ricerca-prodotti', 'Catalogo') }}</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

	<form action="#result" method="get">

	<div class="row">

		<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-search"></i> Filtri</div>

		<div class="panel-body prodotto-form">

		{{ Form::label('IDAreaScientifica', 'Area scientifica:') }}
		{{
			Form::select('IDAreaScientifica', GuiUtils::scientificAreaList(), Input::get('IDAreaScientifica'));
		}}
		<br>
		<br>

		{{ Form::label('IDAreaScientifica', 'Tipologia del prodotto:') }}
		{{
			Form::select('Tipologia', GuiUtils::productTipologies(), Input::get('Tipologia'));
		}}
		<br>
		<br>

		{{ Form::label('DataInizio', 'Dal:') }}
		<input type="date" name="DataInizio" value={{\Input::get('DataInizio')}}>
		<br>
		<br>

		{{ Form::label('DataFine', 'Al:') }}
		<input type="date" name="DataFine" value={{\Input::get('DataFine')}}>
		<br>
		<br>

		{{ Form::label('Autore', 'Cognome autore:') }}
		{{
			Form::Text('Autore', Input::get('Autore'), [
				'placeholder' => 'Rossi',
				'id' => 'autore'
			])
		}}
		<br>
		<br>

		{{ Form::label('Titolo', 'Titolo:') }}
		{{
			Form::Text('Titolo', Input::get('Titolo'), [
				'placeholder' => 'Titolo'
			])
		}}
		<br>
		<br>

		<button type="submit" class="btn btn-default" value="Inserisci">Cerca</button>

	</form>

	</div></div>

</div>

<div id="result" class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-search"></i> Prodotti</div>

		<div class="panel-body">

			<div class="table-responsive">

				<table class="table table-hover">

					<thead>
						<tr>
							<th class="header">Titolo</th>
							<th class="header">Azioni</th>
						</tr>
					</thead>

					@if (empty($prodotti[0]))
						<tr>
							<td>Nessun prodotto di ricerca trovato!</td>
							<td></td>	
						</tr>
					@endif

					@foreach ($prodotti as $prodotto)
					<tr>

						
						<td>
							<p><a href="{{ route('visualizza-prodotto', ['id' => $prodotto->id]) }}">
								{{{ $prodotto->titolo_del_prodotto_di_ricerca }}}
							</a>
							</p>
							<small>		
								@foreach ($prodotto->ricercatori as $ricercatore)
									{{{ $ricercatore->cognome }}} {{{ $ricercatore->nome }}},  
								@endforeach
							</small>
						</td>

						<td>
							@if (Auth::user()->tipologia == Utente::AMMINISTRATORE ||ResearchProduct::verificaPermessi(Auth::user()->id, $prodotto->id))
				                	<li>
				                		<a class="delete" href="{{ route('elimina-prodotto', ['id' => $prodotto->id]) }}">  Elimina</a>
				                	</li>
				                	<li>
				               			<a href="{{ route('modifica-prodotto', ['id' => $prodotto->id]) }}">  Modifica</a>
				                	</li>
			                @endif
						</td>
							
					</tr>	
					@endforeach

				</table>

			</div>
		</div>
	</div>
</div>

					{{-- Piccolo Trick --}}


					{{ $prodotti->appends(Input::except(array('page')))->links() }}

					{{ HTML::script('js/jquery-1.10.2.min.js') }}
					{{ HTML::script('js/script.js') }}


@stop