@extends('layout/master')

@section('navpath')

<div class="row">
      	<div class="col-lg-12">
			<h1>Catalogo <small> gestione permessi</small></h1>
        		<ol class="breadcrumb">
          			<li><a href="{{ url('/')}}">Home</a></li>
        			<li>{{ link_to_route('ricerca-prodotti', 'Catalogo') }}</li>
          			<li class="active">gestione permessi</li>
        		</ol>
      	</div>
</div>

@stop

@section('content')

<div class="row">

	<div class="panel panel-primary">

		<div class="panel-heading"><i class="fa fa-tasks"></i> I miei prodotti</div>

		<div class="panel-body">
	{{ Form::open([
			'route' => 'gestione-permessi',
			'method' => 'post'
			]) }}
	{{ Form::text('nomeUtente', null, array(
				'placeholder' => 'Nome utente',
				'autofocus' => 'true'
				)) }}
	{{ Form::text('titoloProdotto', null, array(
				'placeholder'=>'Titolo del Prodotto'
				)) }}<br>
	{{ Form::submit('Cambia Permessi')}}
	{{ Form::reset('Annulla')}}
	{{ Form::close() }}
</div>
</div>
</div>
@stop