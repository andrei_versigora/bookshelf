<?php

class GuiUtils
{


	public static function scientificAreaList($withEmpty = true)
	{
		$models = ScientificArea::all();
		$aree = ($withEmpty) ? ['' => '...'] : [];
		foreach ($models as $area) {
			$id = $area->id;
			$nome = $area->descrizione;
			$aree = array_merge($aree, [
				"$id " => "$nome"
			]);
		}
		return $aree;
	}

	public static function productTipologies($withEmpty = true)
	{
		$list = [
			'articolo_su_rivista' => 'Articolo su rivista',
			'atto_di_congresso' => 'Atto di congresso',
			'libro_capitolo' => 'Libro - Capitolo',
			'commento' => 'Commenti',
			'traduzione' => 'Traduzione',
			'edizione_critica' => 'Edizione criticha',
			'altro' => 'Altro',
		];

		if ($withEmpty) {
			$list = array_merge(['' => '...'], $list);
		}

		return $list;
	}

	public static function success($message)
	{
		return \View::make('assert.success')->with('message', $message);
	}

	public static function error($message)
	{
		return \View::make('assert.error')->with('error', $message);
	}

}