<?php

class AuthUtils
{
	/**
	 * Funzione d'ausilio che genera la stringa del filtro di sicurezza.
	 *
	 * @return string filtro di autenticazione/login
	 * @author 
	 **/
	public static function auth($roles = [])
	{
		$filter = 'auth';
		for ($i=0; $i < count($roles); $i++) {
			$filter .= ($i == 0) ? ':' : '-';
			$filter .= $roles[$i];
		}
		return $filter;
	}

	/**
	 * Questa funzione restituisce true se l'utente loggato è un amministratore
	 * false altrimenti.
	 *
	 * @return bool
	 */
	public static function amministratore() {
		if (Auth::guest()) {
			return false;
		}

		return Auth::user()->tipologia == \Utente::AMMINISTRATORE;
	}

	/**
	 * Questa funzione restituisce true se l'utente loggato è un ricercatore
	 * false altrimenti.
	 *
	 * @return bool
	 */
	public static function ricercatore() {
		if (Auth::guest()) {
			return false;
		}

		return Auth::user()->tipologia == \Utente::RICERCATORE;
	}

	/**
	 * Questa funzione restituisce true se l'utente loggato è un responsabile di
	 * dipartimento false altrimenti.
	 *
	 * @return bool
	 */
	public static function resDipartimento() {
		if (Auth::guest()) {
			return false;
		}

		return Auth::user()->tipologia == \Utente::RES_DIPARTIMENTO;
	}

	/**
	 * Questa funzione restituisce true se l'utente loggato è un responsabile del
	 * settore disciplinare false altrimenti.
	 *
	 * @return bool
	 */
	public static function resDisciplinare() {
		if (Auth::guest()) {
			return false;
		}

		return Auth::user()->tipologia == \Utente::RES_DISCIPLINARE;
	}

	/**
	 * Questa funzione restituisce true se l'utente loggato è un direttore del
	 * settore disciplinare false altrimenti.
	 *
	 * @return bool
	 */
	public static function dirDisciplinare() {
		if (Auth::guest()) {
			return false;
		}

		return Auth::user()->tipologia == \Utente::DIR_DISCIPLINARE;
	}

	/**
	 * Questa funzione restituisce true se l'utente loggato è un responsabile di
	 * ateneo false altrimenti.
	 *
	 * @return bool
	 */
	public static function resAteneo() {
		if (Auth::guest()) {
			return false;
		}

		return Auth::user()->tipologia == \Utente::RES_ATENEO;
	}

}