<?php

return [

	/*
	|--------------------------------------------------------------------------
	| ATTIVA LA VALIDAZIONE "RISTRETTA"
	|--------------------------------------------------------------------------
	|
	| Se la validazione ristretta è attiva, solo i ricercatori con permessi di
	| modifica di un prodotto di ricerca possono sottomettere il prodotto al
	| processo di validazione. Se non è attiva, tutti gli autori, anche senza
	| permessi di modifica possono farlo.
	|
	*/

	'strictvalidation' => true
];