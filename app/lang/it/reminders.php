<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "La passwords deve essere di sei caratteri e corrispondere al campo di conferma.",

	"user"     => "Non &egrave; esiste un utente con l'indirizzo e-mail specificato.",

	"token"    => "Il token per il reset della password non &egrave; valido.",

);