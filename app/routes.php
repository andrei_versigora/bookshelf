<?php



/*
|--------------------------------------------------------------------------
| Test Routes
|--------------------------------------------------------------------------
| 
| Di seguito sono specificate le route da utilizzare a solo scopo di test
| e che dovranno essere eliminate prima della consegna del sistema.
|
*/


/*
 *
 */
Route::get('/test', function ()
{
	$dettagli = \Other::find(2);
	return $dettagli->prodotto;
	
});

/*
 *	Stampa l'hash in bcrypt della parola 'password'
 */
Route::get('/password', function ()
{
	return Hash::make('password');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::get('/', [
	// Specifica il controller da richiamare
	'uses' => 'HomeController@showWelcome',
	// Specifica le tipologie di utenti che possono accedere alla funzionalità
	'as' => 'home',
	'before' => AuthUtils::auth(),
]);



// Gestione utenti

Route::get('login', [
	'uses' => 'GestioneUtenti\UserManagementController@visualizzaLoginPage',
	'as' => 'form-login'
]);

Route::post('login', [
	'uses' => 'GestioneUtenti\UserManagementController@login',
	'as' => 'login'
]);

Route::get('logout', [
	'uses' => 'GestioneUtenti\UserManagementController@logout',
	'as' => 'logout'
]);

/*
 *  Visualizza tutti gli utenti.
 */
/*
Route::get('utenti/visualizza', [
	'uses' => 'GestioneUtenti\UserManagementController@VisualizzaUtenti',
	'as' => 'visualizza-utenti',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);
*/

/*
 *  Amministratore: Inserimento di un nuovo utente.
 */
Route::post('utenti/inserisci', [
	'uses' => 'GestioneUtenti\UserManagementController@InserisciUtente',
	'as' => 'inserisci-utente',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *  Amministratore: View inserimento di un nuovo utente.
 */
Route::get('utenti/inserisci', [
	'uses' => 'GestioneUtenti\UserManagementController@GetInsView',
	'as' => 'form-inserisci-utente',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *  Amministratore/Ricercatore: Modifica utente.
 */
Route::post('utenti/modifica', [
	'uses' => 'GestioneUtenti\UserManagementController@ModificaUtente',
	'as' => 'modifica-utente',
	'before' => AuthUtils::auth([
		Utente::RICERCATORE,
		Utente::AMMINISTRATORE
		]),
]);

/*
 *  Amministratore/Ricercatore: View Modifica utente.
 */
Route::get('utenti/modifica', [
	'uses' => 'GestioneUtenti\UserManagementController@GetModView',
	'as' => 'form-modifica-utente',
	'before' => AuthUtils::auth([
		Utente::RICERCATORE,
		Utente::AMMINISTRATORE
		]),
]);

/*
 *  Visualizza la view di Modifica dei dati dell'utente loggato.
 */
Route::get('utenti/modifica/me', [
	'as' => 'form-modifica-utente-me',
	'before' => AuthUtils::auth(),
	function ()
	{
		return Redirect::route('form-modifica-utente', ['id' => Auth::user()->id]);
	}
]);

/*
 *  Amministratore: View seleziona utente per modifica.
 */
Route::get('utenti', [
	'uses' => 'GestioneUtenti\UserManagementController@GetSelView',
	'as' => 'form-seleziona-utente',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *  Amministratore: Ricerca utente.
 */
Route::get('utenti/ricerca', [
	'uses' => 'GestioneUtenti\UserManagementController@RicercaUtenti',
	'as' => 'ricerca-utenti',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *  Amministratore: View per scegliere utente da rimuovere.
 */
Route::get('utenti/rimuovi', [
	'uses' => 'GestioneUtenti\UserManagementController@GetDelView',
	'as' => 'form-rimuovi',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);


/*
 *  Amministratore: Seleziona utente per rimuoverlo.
 */
Route::post('utenti/rimuovi', [
	'uses' => 'GestioneUtenti\UserManagementController@Rimuovi',
	'as' => 'rimuovi',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *  Amministratore: View di conferma per rimuovere utente.
 */
Route::get('utenti/rimuoviutente', [
	'uses' => 'GestioneUtenti\UserManagementController@GetDelUserView',
	'as' => 'form-rimuovi-utente',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *  Amministratore: Rimuovi utente.
 */
Route::post('utenti/rimuoviutente', [
	'uses' => 'GestioneUtenti\UserManagementController@RimuoviUtente',
	'as' => 'rimuovi-utente',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *	Visualizza tutti prodotti dell'utente loggato.
 */
Route::get('prodotti', [
	'uses' => 'GestioneProdotti\ProductManager@visualizzaprodotti',
	'as' => 'prodotti',
	'before' => AuthUtils::auth(),
]);

/*
 *	Visualizza un singolo prodotto
 */
Route::get('prodotti/visualizza', [
	'uses' => 'GestioneProdotti\ProductManager@visualizzaprodotto',
	'as' => 'visualizza-prodotto',
	'before' => AuthUtils::auth(),
]);

/*
 *	Ricercatore: Visualizza la form per l'inserimento di un prodotto
 */
Route::get('prodotti/inserisci', [
	'uses' => 'GestioneProdotti\ProductManagerControllerAbstr@getInsertView',
	'as' => 'form-inserisci-prodotto',
	'before' => AuthUtils::auth([Utente::RICERCATORE]),
]);

/*
 *	Ricercatore: Inserisci un prodotto
 */
Route::post('/prodotti/inserisci', [
	'uses' => 'GestioneProdotti\ProductManagerControllerAbstr@addProduct',
	'as' => 'inserisci-prodotto',
	'before' => AuthUtils::auth([Utente::RICERCATORE]),
]);

/*
 *	Amministratore/Ricercatore: Visualizza la form per la modifica di un prodotto
 */
Route::get('/prodotti/modifica', [
	'uses' => 'GestioneProdotti\ProductManagerControllerAbstr@getModifyView',
	'as' => 'form-modifica-prodotto',
	'before' => AuthUtils::auth([
			// NOTA BENE: La lista degli utenti che possono accedere al route
			// deve essere in ordine alfabetico.
			Utente::AMMINISTRATORE,
			Utente::RICERCATORE,
		]),
]);

/*
 *	Amministratore/Ricercatore: Modifica il prodotto
 */
Route::post('/prodotti/modifica', [
	'uses' => 'GestioneProdotti\ProductManagerControllerAbstr@modifyProduct',
	'as' => 'modifica-prodotto',
	'before' => AuthUtils::auth([
			Utente::AMMINISTRATORE,
			Utente::RICERCATORE,
		]),
]);

/*
 *	Amministratore/Ricercatore: Elimina un prodotto.
 */
Route::get('/prodotti/elimina', [
	'uses' => 'GestioneProdotti\ProductManager@deleteProduct',
	'as' => 'elimina-prodotto',
	'before' => AuthUtils::auth([
			Utente::AMMINISTRATORE,
			Utente::RICERCATORE,
		]),
]);

/*
 *	Ricerca dei prodotti
 */
Route::get('/prodotti/ricerca', [
	'uses' => 'GestioneProdotti\SearchProductController@ricercaprodotti',
	'as' => 'ricerca-prodotti',
	'before' => AuthUtils::auth(),
]);

/*
 *	Amministratore: Visualizza la form per la gestione dei permessi
 */
Route::get('prodotti/permessi', [
	'uses' => 'GestioneProdotti\ProductManager@gestionePermessiView',
	'as' => 'form-gestione-permessi',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *	Amministratore: Gestione dei permessi
 */
Route::post('prodotti/permessi', [
	'uses' => 'GestioneProdotti\ProductManager@gestionePermessi',
	'as' => 'gestione-permessi',
	'before' => AuthUtils::auth([Utente::AMMINISTRATORE]),
]);

/*
 *	Amministratore/Ricercatore: Visualizza la form per la gestione del periodo di valutazione
 */
Route::get('periodovalutazione',[
	'uses'=>'GestioneValutazione\ManagementValuationPeriod@setValuationPeriodForm',
	'as'=>'form-gestione-periodo-valutazione',
	'before'=> AuthUtils::auth([
		Utente::AMMINISTRATORE,
		Utente::RICERCATORE
		]),
]);

/*
 *	Amministratore/Ricercatore: Gestione del periodo di valutazione
 */
Route::post('periodovalutazione',[
	'uses'=>'GestioneValutazione\ManagementValuationPeriod@setValuationPeriod',
	'as'=>'gestione-periodo-valutazione',
	'before'=> AuthUtils::auth([
		Utente::AMMINISTRATORE,
		Utente::RICERCATORE
		]),
]);

/**
 *	Amministratore/Ricercatore: Visualizza i periodi della Valutazione per l'amministratore ed il periodo attuale della Valutazione per il Ricercatore
 */
Route::get('visualizzaperiodi',[
	'uses'=>'GestioneValutazione\ManagementValuationPeriod@displaysValutationPeriod',
	'as'=>'periodi-valutazione',
	'before'=> AuthUtils::auth([
		Utente::AMMINISTRATORE,
		Utente::RICERCATORE
		]),
]);

/**
* Visualizza prodotti da sottomettere alla valutazione
*/
Route::get('valutazione/scegli',[
	'uses' => 'GestioneValutazione\EvalutationProductSelectionController@listaProdottiDaSottomettere',
	'as' => 'lista-prodotti-da-valutare',
	'before' => AuthUtils::auth([
		Utente::RICERCATORE
		])
]);

/**
* Inserisce la lista di prodotti scelti nel database
**/
Route::post('valutazione/scegli',[
	'uses' => 'GestioneValutazione\EvalutationProductSelectionController@submit',
	'as' => 'inserimento-lista-prodotti-da-valutare',
	'before' => AuthUtils::auth([
		Utente::RICERCATORE
		])
]);

/**
*	Gestione valutazione
*	seleziona prodotto
*/
Route::post('prodotti/valutazione', [
	'uses' => 'GestioneValutazione\EvalutationProductSelectionController@selezionaProdotto',
	'as' => 'seleziona-prodotto-valutazione',
	'before' => AuthUtils::auth([Utente::RICERCATORE]),
]);

/**
*	Gestione valutazione
*	deseleziona prodotto
*/
Route::post('prodotti/valutazione', [
	'uses' => 'GestioneValutazione\EvalutationProductSelectionController@deselezionaProdotto',
	'as' => 'deseleziona-prodotto-valutazione',
	'before' => AuthUtils::auth([Utente::RICERCATORE]),
]);

/**
* Visualizza prodotti sottomessi alla valutazione
*/
Route::get('listaprodotti',[
	'uses' => 'GestioneValutazione\ProductValutationController@listaProdottiDaValutare',
	'as' => 'lista-prodotti',
	'before' => AuthUtils::auth([
		Utente::DIR_DISCIPLINARE,
		Utente::RES_ATENEO
		])
]);

/**
* Visualizza conflitti
*/
Route::get('visualizzaconflitti',[
	'uses' => 'GestioneValutazione\ProductValutationController@visualizzaProdottiConflitti',
	'as' => 'visualizza-conflitti',
	'before' => AuthUtils::auth([
		Utente::DIR_DISCIPLINARE,
		Utente::RES_ATENEO
		])
]);

/*
Route::get('controllaconflitti',[
	'uses' => 'GestioneValutazione\ProductValutationController@controllaConflitti',
	'as' => 'controlla-conflitti',
	'before' => AuthUtils::auth([
		Utente::DIR_DISCIPLINARE,
		Utente::RES_ATENEO
		])
]);
*/
Route::post('listaProdottiDaValutareXRicercatore',[
	'uses' => 'GestioneValutazione\ProductValutationController@listaProdottiDaValutareXRicercatore',
	'as' => 'lista-prodotti-da-valutare-ricercatore',
	'before' => AuthUtils::auth([
		Utente::DIR_DISCIPLINARE
		])
]);

/**
*	La Selezione dei prodotti per la valutazione dal Responsabile dell’area scientifica
*	
*/
/*
Route::post('selezionaProdotto',[
	'uses' => 'GestioneValutazione\ProductValutationController@selezionaProdotto',
	'as' => 'seleziona-prodotto-processo-valutazione',
	'before' => AuthUtils::auth([
		Utente::DIR_DISCIPLINARE
		])
]);
*/
Route::post('eliminaProdotto',[
	'uses' => 'GestioneValutazione\ProductValutationController@eliminaProdotto',
	'as' => 'elimina-prodotto-processo-valutazione',
	'before' => AuthUtils::auth([
		Utente::DIR_DISCIPLINARE,
		Utente::RES_ATENEO
		])
]);

/**
*	La invio dei prodotti per la valutazione dal Responsabile dell’ateneo
*/
Route::post('inviaLista',[
	'uses' => 'GestioneValutazione\ProductValutationController@invioAlVQR',
	'as' => 'invio-prodotto-processo-valutazione',
	'before' => AuthUtils::auth([
		Utente::RES_ATENEO
		])
]);


Route::get('/api/utenti', 'Api@utenti');
Route::get('/api/aggiungiricercatore', 'Api@aggiungiRicercatore');

/*
 *	Visualizza i prodotti da poter validare, tutto dinamicamente in base al tipo di utente loggato tra: Amministratore
 * 	Res dipartimento e Res Concilio Disciplinare
 */
Route::get('validazione/scegli',[
	'uses' => 'GestioneValidazione\ValidationManager@selectProductsToBeValidateList',
	'as' => 'lista-prodotti-da-validare',
	'before' => AuthUtils::auth([
		Utente::RICERCATORE
		])
]);

/*
 *	Visualizza i prodotti da poter validare, tutto dinamicamente in base al tipo di utente loggato tra: Amministratore
 * 	Res dipartimento e Res Concilio Disciplinare
 */
Route::post('validazione/scegli',[
	'uses' => 'GestioneValidazione\ValidationManager@selectProductsToBeValidate',
	'as' => 'seleziona-prodotto-da-validare',
	'before' => AuthUtils::auth([
		Utente::RICERCATORE
		])
]);

/*
 *	Visualizza i prodotti da poter validare, tutto dinamicamente in base al tipo di utente loggato tra: Amministratore
 * 	Res dipartimento e Res Concilio Disciplinare
 */
Route::get('validazione/visualizza',[
	'uses' => 'GestioneValidazione\ValidationManager@viewProductToBeValidate',
	'as' => 'visualizza-prodotti-da-validare',
	'before' => AuthUtils::auth([
		Utente::RES_DIPARTIMENTO,
		Utente::RES_DISCIPLINARE,
		])

]);
/*
 *	Visualizza il prodotto nello specifico e permette di validarlo o invalidarlo tramite delle anchor
 */
Route::any('validazione/visualizza={id}',[
	'uses' => 'GestioneValidazione\ValidationManager@viewProduct',
	'as' => 'visualizza-prodotto-da-validare',
	'before' => AuthUtils::auth([
		Utente::RES_DIPARTIMENTO,
		Utente::RES_DISCIPLINARE,
		])

]);


/*
 *	Route che porta al controller che si occupa della validazione del prodotto, tutto in base all'utente loggato
 */
Route::any('validazione/valida={id}',[
	'uses' => 'GestioneValidazione\ValidationManager@validProduct',
	'as' => 'valida-prodotto',
	'before' => AuthUtils::auth([
		Utente::RES_DIPARTIMENTO,
		Utente::RES_DISCIPLINARE,
		Utente::RICERCATORE,
		])
]);

/*
 *	Route che porta al controller che si occupa d'invalidare un prodotto, tutto in base all'utente loggato
 */
Route::any('validazione/invalida={id}',[
	'uses' => 'GestioneValidazione\ValidationManager@invalidProduct',
	'as' => 'invalida-prodotto',
	'before' => AuthUtils::auth([
		Utente::RES_DIPARTIMENTO,
		Utente::RES_DISCIPLINARE,
		])
]);

Route::get('validazione/impostaperiodo',[
	'uses' => 'GestioneValidazione\ValidationManager@setValidationPeriodForm',
	'as' => 'form-imposta-periodo-validazione',
	'before' => AuthUtils::auth([
		Utente::AMMINISTRATORE
		])
]);

Route::post('validazione/impostaperiodo',[
	'uses' => 'GestioneValidazione\ValidationManager@setValidationPeriod',
	'as' => 'imposta-periodo-validazione',
	'before' => AuthUtils::auth([
		Utente::AMMINISTRATORE
		])
]);


