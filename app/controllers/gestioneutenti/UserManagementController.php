<?php

namespace GestioneUtenti;
/**
 * Classe per la gestione degli utenti del sistema.
 *
 * @author Giuseppe Sorrentino <g.sorrentino60@studenti.unisa.it>
 */
class UserManagementController extends \Controller
{
    /**
     * Visualizza la lista di tutti gli utenti del sistema.
     *
     * @return view Visualizzazione utenti del sistema.
	 */
	public function VisualizzaUtenti()
	{	
		$arrayUtenti = \Utente::all();
		return \View::make('gestioneutentigui/VisualizzaUtenti')->with('utenti', $arrayUtenti);
	}

	/**
	 * Visualizza la pagina di login.
	 *
	 * @return view Visualizzazione pagina per il login.
	 */
	public function visualizzaLoginPage()
	{
		return \View::make('gestioneutentigui/loginform');	
	}

	/**
	 * Prova ad effettuare il login.
	 *
	 * @param string nomeUtente
	 * @param string password
	 * @return redirect homepage se login corretto, View login form se errato.
	 */
	public function login()
	{
		// Le regole che i parametri devono rispettare
		$rules = array(
			'nomeUtente' => 'required',
			'password' => 'required|min:8|max:20'
		);

		// Verifica che i parametri siano conformi alle regole specificate...
	    $validator = \Validator::make(\Input::all(), $rules);

	    // ...se non sono conformi
	    if ($validator->fails())
	    {	
	        $rvalue = \Redirect::route('form-login')
	        	->withInput(\Input::except('password'))
	        	->withErrors($validator);
        	return $rvalue;
	    }

	    // ...altrimenti prova a effettuare il login
        $nomeUtente = \Input::get('nomeUtente');
	    $password = \Input::get('password');
	    if (\Auth::attempt(['username' => $nomeUtente, 'password' => $password], true)){
		    return \Redirect::intended('/');
		}

        return $this->visualizzaLoginPage()
        	->withInput(\Input::except('password'))
        	->with('loginError', 'Invalid username or password');
	}

	/**
	 * Effettua il logout.
	 *
	 * @return redirect homepage 
	 */
	public function logout()
	{
		\Auth::logout();
		return \Redirect::to('/');	
	}


	/**
	 * Inserisce un utente nel sistema.
	 * 
	 * @param string nome
	 * @param string cognome
	 * @return view Visualizza utenti se corretto, Form inserisci utente se errato.
	 */
	public function InserisciUtente()
	{	
        // Le regole che i parametri devono rispettare
		$rules = array(
		    'txt_password' => 'min:8|max:16',
		    'txt_nome' => 'required|alpha|max:40',
		    'txt_cognome' => 'required|alpha|max:40',
		    'txt_email' => 'email|max:40',
		    'txt_telefono' => 'max:15',
		    'txt_cellulare' => 'numeric|digits:10',
		    'txt_via' => 'max:40',
		    'txt_numcivico'=>'numeric',
		    'txt_citta' => 'max:40',
		    'txt_provincia' => 'digits:2',
		);

		// Verifica che i parametri siano conformi alle regole specificate...
	    $validator = \Validator::make(\Input::all(), $rules);

	    // ...se non sono conformi
	    if ($validator->fails()){	
	        // Se i parametri non sono corretti.
	        return \Redirect::route('form-inserisci-utente')
	            ->withInput(\Input::all())
	    	->withErrors($validator);
	    }

        \DB::transaction(function(){
        $utente= new \Utente;

        $utente->username = \Input::get('txt_username');
        $pass=\Input::get('txt_password');
        $utente->password = \Hash::make($pass);
        $utente->nome = \Input::get('txt_nome');
        $utente->cognome = \Input::get('txt_cognome');
	    $utente->email = \Input::get('txt_email');
        $utente->telefono = \Input::get('txt_telefono');
        $utente->cellulare = \Input::get('txt_cellulare');
        $utente->via = \Input::get('txt_via');
        $utente->num_civico = \Input::get('txt_numcivico');
        $utente->città = \Input::get('txt_citta');
        $utente->provincia = \Input::get('txt_provincia');
        $utente->data_servizio = \Input::get('txt_dataservizio');
        $utente->tipologia = \Input::get('txt_tipologia');
        $areascientifica=\Input::get('txt_areascientifica');
    	if($areascientifica=="NULL")
    	$utente->id_area_scientifica=NULL;
        else
    	$utente->id_area_scientifica=$areascientifica;

        $utente->save();
        });

        $arrayUtenti = \Utente::all();
		return \View::make('gestioneutentigui/RicercaUtenti')->with('utenti', $arrayUtenti);
	
	}

    /**
     * Funzione che restituisce la view per l'inserimento dell'utente.
     *
     * @return view Form per l'inserimento di un utente.
     */
    public function GetInsView()
    {
        return \View::make('gestioneutentigui/InserisciUtente');
    }	

    /**
     * Funzione che restituisce la view di selezione per la modifica di un utente.
     *
     * @return view Form per selezionare un utente da modificare.
     */
    public function GetSelView()
    {
	    return \View::make('gestioneutentigui/SelezionaUtente');
    }	

    /**
	 * Modifica di un utente del sistema.
	 *
	 * @param string nome
	 * @param string cognome
	 * @return view Visualizzazione utenti se la modifica è corretta, Form modifica utente se errata.
	 */
	public function ModificaUtente()
	{
        // Le regole che i parametri devono rispettare
		$rules = array(
			'nome' => 'required|max:40|alpha',
			'cognome' => 'required|max:40|alpha',
			'email' => 'email|max:40',
			'telefono' => 'max:15',
			'cellulare' => 'numeric|digits:10',
			'via' => 'max:40',
			'citta' => 'max:40',
			'provincia' => 'digits:2',
			'password' => 'confirmed|min:8'
		);
		
		// Verifica che i parametri siano conformi alle regole specificate.
	    $validator = \Validator::make(\Input::all(), $rules);


	    // ...se non sono conformi
	    if ($validator->fails()) {	
		    // Se i parametri non sono corretti.
	        $rvalue = \Redirect::back()
		        ->withInput()
	        	->withErrors($validator)
	        	->with('id',\Input::get('id'))
	            ->with('message','');
        	return $rvalue;
	    }

        $utente = \Utente::find(\Input::get('id'));

        if (!empty(\Input::get('password'))) {
        	$utente->password = \Hash::make(\Input::get('password'));
        }
	
		$utente->nome = \Input::get('nome');
    	$utente->cognome = \Input::get('cognome');
		$utente->email = \Input::get('email');
    	$utente->telefono = \Input::get('telefono');
    	$utente->cellulare = \Input::get('cellulare');
    	$utente->via = \Input::get('via');
    	$utente->num_civico = \Input::get('num_civico');
    	$utente->città = \Input::get('città');
    	$utente->provincia = \Input::get('provincia');
    	$utente->data_servizio = \Input::get('data_servizio');
    	$utente->tipologia=\Input::get('tipologia');
    	$areascientifica=\Input::get('id_area_scientifica');
    	if($areascientifica=="NULL")
    	$utente->id_area_scientifica=NULL;
        else
    	$utente->id_area_scientifica=$areascientifica; 
    	$utente->save();
        
        // Se l'utente loggato è un ricercatore, gli verrà concessa la visualizzazione solo del suo account
        if(\Auth::user()->tipologia == \Utente::RICERCATORE)
        	$arrayUtenti = \Utente::where('id', '=', \Auth::user()->id)->get();
        else 
            // Se l'utente loggato è un amministratore invece, vedrà tutti gli utenti
            $arrayUtenti = \Utente::all();
		return \View::make('gestioneutentigui/VisualizzaUtenti')->with('utenti', $arrayUtenti);	
	}

    /**
     * AMMINISTRATORE/RICERCATORE Funzione che restituisce la view di modifica di un utente.
     *
     * @return view Form per modificare l'utente scelto, errore se id utente non definito o non si hanno i permessi.
     */
    public function GetModView()
    {
        if (!\Input::has('id')) {
            return "ERRORE: ID utente non definito.";
	    }
	    $idUtente = \Input::get('id');
	    // Controllo, se utente è ricercatore, può modificare solo il proprio account.
	    if((\Auth::user()->tipologia == \Utente::RICERCATORE) && (\Auth::user()->id!=$idUtente))
	        return "ERRORE: Non hai permessi per modificare l'account di questo utente.";

        $utente = \Utente::find($idUtente);
        if(count($utente)==0)
        	return "IDUtente non trovato!";
        return \View::make('gestioneutentigui/ModificaUtente')->with('utente', $utente);
    }
    
    /**
 	 * Ricerca di uno o più utenti, attraverso i campi: area scientifica, nome e cognome.
 	 *
 	 * @return view Form Visualizzazione utenti con i risultati della ricerca se andata a buon fine, Form per la ricerca se parametri errati. 
     */
    public function RicercaUtenti()
    {   
	    $nome = \Input::get('nome');
	    $cognome = \Input::get('cognome');
	    $areascientifica = \Input::get('id_area_scientifica');
	    
        $query = \Utente::query();

        if (!empty($areascientifica)) {
            $query = $query->where('id_area_scientifica', '=', $areascientifica);
        }

        if (!empty($nome)) {
            $query = $query->where('nome', 'LIKE', "%$nome%");
        }

        if (!empty($cognome)) {
            $query = $query->where('cognome', 'LIKE', "%$cognome%");
        }

        $utentiRic = $query->orderBy('cognome')->orderBy('nome')->orderBy('username')->get();
        return \View::make('gestioneutentigui/RicercaUtenti')->with('utenti', $utentiRic);
    }

    /**
     * Funzione che restituisce la view di visualizzazione di tutti gli utenti per la rimozione.
     *
     * @return view Selezione dell'utente da rimuovere.
     */
    public function GetDelView()
    {
        return \View::make('gestioneutentigui/Rimuovi');
    }

    /**
     * Funzione che restituisce la view per confermare l'eliminazione di un utente.
     *
     * @return view Rimozione di un utente dal sistema, errore se utente non presente nel sistema.
     */
    public function GetDelUserView()
    {
    	if (!\Input::has('id')) {
            return "ERRORE: ID utente non definito";
	    }
        $idUtente = \Input::get('id');
        $utente = \Utente::where('id','=', $idUtente)->get();
        if (count($utente)==0) return "ERRORE: ID utente non presente nel database";
        return \View::make('gestioneutentigui/RimuoviUtente')->with('utente', \Utente::find($idUtente));
    }

    /**
 	 * Rimozione di un utente dal sistema.
 	 *
 	 * @param integer id
 	 * @return view Visualizzazione utenti se rimozione corretta, errore se utente non presente nel sistema.
     */
    public function RimuoviUtente()
    {
       // Le regole che i parametri devono rispettare
		$rules = array(
			'id' => 'required|exists:utente,id',
			);
		
		// Verifica che i parametri siano conformi alle regole specificate.
	    $validator = \Validator::make(\Input::all(), $rules);

	    $idUtente= \Input::get('id');
	    // ...se non sono conformi
	    if ($validator->fails()) {	
	    // Se i parametri non sono corretti.
        return 'ERRORE: ID non presente nel database degli utenti.';
        }

        $utente = \Utente::find(\Input::get('id'));
        $utente->delete();

        $arrayUtenti = \Utente::all();
		return $this->RicercaUtenti();	
    }
}
