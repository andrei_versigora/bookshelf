<?php

namespace GestioneValidazione;

/**
 *	Classe che si occupa della gestione della validazione ed inserimento e modifica del periodo di validazione
 */
class ValidationManager extends \Controller {

	/**
	 * Visualizza la lista dei prodotti che possono essere inviati alla validazione.
	 *
	 * @return View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */
	public function selectProductsToBeValidateList()
	{	
		$prodotti = [];
		if (\Config::get('settings.strictvalidation')) {
			$prodotti = \Auth::user()->prodottiConPermessi();
		} else {
			$prodotti = \Auth::user()->prodotti();
		}
		
		$prodotti = $prodotti->where('stato_validazione', '=', 'non_validato')->get();
		return \View::make('gestionevalidazionegui.ListaProdotti')->with('prodotti', $prodotti);
	}

	/**
	 * Seleziona un prodotto da sottomettere alla validazione.
	 * Input param:
	 *
	 * @param int $id id del prodotto da sottomettere alla validazione
	 *
	 * @return View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */
	public function selectProductsToBeValidate()
	{	
		$rules = [
			// Aggiungere il requisito che il prodotto esista nel DB
			'id' => 'required|numeric|exists:prodotto_di_ricerca'
		];

		$validator = \Validator::make(\Input::all(), $rules);
		
		if($validator->fails())
		{
			return \Redirect::route('visualizza-prodotti-da-validare')->withErrors($validator);
		}
		// Valida input

		$id = \Input::get('id');

		$prodotto = \ResearchProduct::find($id);
		
		// cambia lo stato del prodotto ecc.

		return \Redirect::back();
	}

	/**
	 * Visualizza la pagina di impostazione del periodo di validazione.
	 *
	 * @return View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */
	public function setValidationPeriodForm()
	{	
		return \View::make('gestionevalidazionegui.ImpostaPeriodoValidazione');
	}

	/**
	 * Funzione che permette la modifica e l'inserimento del periodo di validazione
	 * @return  View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 *	
	 */
	public function setValidationPeriod()
	{	
		// Le regole che i parametri devono rispettare
		$rules = array(
					'data_inizio' => 'required|date',
					'data_fine' => 'required|date',
					'data_inizio_consiglio' => 'required|date',
					'data_fine_consiglio' => 'required|date',
					'data_inizio_concilio' => 'required|date',
					'data_fine_concilio' => 'required|date'
			);

		// Verifica che i parametri siano conformi alle regole specificate...
	    $validator = \Validator::make(\Input::all(), $rules);

	    // ...se non sono conformi
	    if ($validator->fails())
	    {
	    	return \Redirect::route('imposta-periodo-validazione')
	    	->withInput(\Input::all())
	    	->withErrors($validator);
	    }

	    // Setta periodo di validazione
	    $periodo = \ValidationPeriod::first();
	    if (empty($periodo)){
	    $periodo = new \ValidationPeriod;
		}
	    $periodo->data_inizio = \Input::get('data_inizio');
	    $periodo->data_fine = \Input::get('data_fine');
	    $periodo->data_inizio_consiglio = \Input::get('data_inizio_consiglio');
	    $periodo->data_fine_consiglio = \Input::get('data_fine_consiglio');
	    $periodo->data_inizio_concilio = \Input::get('data_inizio_concilio');
	    $periodo->data_fine_concilio = \Input::get('data_fine_concilio');
	    $periodo->save();

	    return \GuiUtils::success('Periodo di valutazione impostato correttamente.');
	    
	    // return \View::make('gestionevalidazionegui.PeriodoValidazioneCorretto');//restituisce la view di successo
	}


	/** 
	 * Visualizza la lista dei prodotti da validare a seconda del responsabile loggato.
	 * @return View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */	
	public function viewProductToBeValidate()
	{	
		
			$order=\Input::get('order');
			if(!isset($order))
				{
					$order='titolo_del_prodotto_di_ricerca';
				}
		
			if (\Auth::user()->tipologia == \Utente::RES_DIPARTIMENTO) {
				if($order != 'Autore'){
				$prodotti = \ResearchProduct::where('stato_validazione','=','in_validazione')->orderBy($order)->get();
				return \View::make('gestionevalidazionegui.ListaProdotti')->with('prodotti',$prodotti);
				}
				else
				{
					$prodotti=\DB::table('prodotto_di_ricerca')
								->where('stato_validazione','=','in_validazione')
								->join('realizzazione','prodotto_di_ricerca.id','=','realizzazione.id_prodotto_di_ricerca')
								->join('utente','realizzazione.id_ricercatore','=','utente.id')
								->orderBy('utente.cognome')
								->groupBy('prodotto_di_ricerca.id')
								->select('prodotto_di_ricerca.id','prodotto_di_ricerca.titolo_del_prodotto_di_ricerca','utente.cognome','prodotto_di_ricerca.descrizione')->get();
								$array=array();
								foreach ($prodotti as $prodotto) {
									$array[]=\ResearchProduct::find($prodotto->id);
									} return \View::make('gestionevalidazionegui.ListaProdotti')->with('prodotti',$array,'order',$order);
				
			} } if (\Auth::user()->tipologia == \Utente::RES_DISCIPLINARE){
						//if($order != 'Autore'){
							$prodotti = \ResearchProduct::where('stato_validazione','validato_dipartimento')->get();
							return \View::make('gestionevalidazionegui.ListaProdotti')->with('prodotti',$prodotti);}}
						/*} else {
								$prodotti=\DB::table('prodotto_di_ricerca')
								->where('stato_validazione','=','in_validazione')
								->join('realizzazione','prodotto_di_ricerca.id','=','realizzazione.id_prodotto_di_ricerca')
								->join('utente','realizzazione.id_ricercatore','=','utente.id')
								->orderBy('utente.cognome')
								->groupBy('prodotto_di_ricerca.id')
								->select('prodotto_di_ricerca.id','prodotto_di_ricerca.titolo_del_prodotto_di_ricerca','utente.cognome','prodotto_di_ricerca.descrizione')->get();
								$array=array();
								foreach ($prodotti as $prodotto) {
									$array[]=\ResearchProduct::find($prodotto->id);
									} return \View::make('gestionevalidazionegui.ListaProdotti')->with('prodotti',$array,'order',$order);
								}
			}
	}
 
 	/**
 	 *	Visualizza il prodotto nel dettaglio facendo decidere se validarlo o meno.
 	 * @param 	int $id Id del prodotto da visualizzare nel dettaglio
 	 * @return 	View
 	 *
 	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
 	 */
	public function viewProduct($id)
	{
		$rules = [
			// Aggiungere il requisito che il prodotto esista nel DB
			'id' => 'required|numeric|exists:prodotto_di_ricerca'
		];

		$validator = \Validator::make(\Input::all(), $rules);
		
		if($validator->fails())
		{
			return \Redirect::route('visualizza-prodotti-da-validare')->withErrors($validator);
		}
		$prodotto = \ResearchProduct::find($id);
		return \View::make('gestionevalidazionegui.visualizzaProdottoValidazione')->with('prodotto',$prodotto);
	}
	    
	    

		


	/**	
	 *	Funzione che in base al tipo di responsabile loggato, valida di conseguenza il prodotto.
	 * @param 	int $id Id del prodotto da validare(in base a colui che valida cambierà il suo stato)
	 * @return 	View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */

	public function validProduct($id)
	{	

		$prodotto = \ResearchProduct::find($id);
		if ($this->checkTime($prodotto)){
			if (\Auth::user()->tipologia == \Utente::RICERCATORE){
			$prodotto->stato_validazione = "in_validazione";
			}
			if (\Auth::user()->tipologia == \Utente::RES_DIPARTIMENTO){
			$prodotto->stato_validazione = "validato_dipartimento";
			} else if (\Auth::user()->tipologia == \Utente::RES_DISCIPLINARE){
			$prodotto->stato_validazione = 'validato_area';
			}
			$prodotto->save();
			return \View::make('gestionevalidazionegui.ValidazioneEffettuata');
		} return \View::make('gestionevalidazionegui.ValidazioneNonEffettuata');
	}
	/**
	 *	Funzione che in base al tipo di responsabile loggato, invalida di conseguenza il prodotto.
	 * @param 	int $id Id del prodotto che sarà invalidato
	 * @return 	View
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */
	public function invalidProduct($id)
	{
		$rules = [
			// Aggiungere il requisito che il prodotto esista nel DB
			'id' => 'required|numeric|exists:prodotto_di_ricerca'
		];

		$validator = \Validator::make(\Input::all(), $rules);
		
		if($validator->fails())
		{
			return \Redirect::route('visualizza-prodotti-da-validare')->withErrors($validator);
		}
		$prodotto=\ResearchProduct::find($id);
		// Invalida prodotto da parte dell'amministratore senza tener conto dei periodi di validazione
		if (\Auth::user()->tipologia == \Utente::AMMINISTRATORE) {
				$prodotto->stato_validazione = "non_validato";
				$prodotto->save();
		}
		if($this->checkTime($prodotto)){
		// Invalida prodotto da parte del responsabile di dipartimento e disciplinare
			if (\Auth::user()->tipologia == \Utente::RES_DIPARTIMENTO|| \Auth::user()->tipologia == \Utente::RES_DISCIPLINARE) {
				$prodotto->stato_validazione = "non_validato";
				$prodotto->save();
				return \View::make('gestionevalidazionegui.InvalidazioneEffettuata');
			}
		} return \View::make('gestionevalidazionegui.InvalidazioneNonEffettutata');

		

	}
	/**
	 *	Funzione che si occupa di controllare se nel momento "now" i periodi di validazione sono ancora validi.
	 * @param 	ResearchProduct $prodotto variabile contenete un oggetto di tipo "ResearchProduct" in cui controllare se il periodo di validazione è valido
	 * @return 	boolean 
	 *
	 * @since   2013-12-31
	 * @author  Massimo Paribelli <m.paribelli@unisa.studenti.it>
	 */
	public function checkTime($prodotto)
	{
		if(\Auth::user()->tipologia == \Utente::RICERCATORE){
			
			$prodotto->id_periodo_validazione = \ValidationPeriod::first()->id;

			$ora = date('Y-m-d');
			$datainizio = \ValidationPeriod::find($prodotto->id_periodo_validazione)->data_inizio;
			$datafine = \ValidationPeriod::find($prodotto->id_periodo_validazione)->data_fine;
				if($ora >= $datainizio && $ora <= $datafine){
					if(\ResearchProduct::verificaPermessi(\Auth::user()->id,$prodotto->id)){
					return true;}// se si è nel lasso temporale esatto
					} else {
					return false;//se non si è nel lasso temporale esatto
					}

			}
		
		if(\Auth::user()->tipologia == \Utente::RES_DIPARTIMENTO){
			$ora = date('Y-m-d');
			$datainizio = \ValidationPeriod::find($prodotto->id_periodo_validazione)->data_inizio_consiglio;
			$datafine = \ValidationPeriod::find($prodotto->id_periodo_validazione)->data_fine_consiglio;
				if($ora >= $datainizio && $ora <= $datafine){
					return true;// se si è nel lasso temporale esatto
				} else {
					return false;//se non si è nel lasso temporale esatto
				}
			}
			if(\Auth::user()->tipologia == \Utente::RES_DISCIPLINARE){
			$ora = date('Y-m-d');
			$datainizio = \ValidationPeriod::find($prodotto->id_periodo_validazione)->data_inizio_concilio;
			$datafine = \ValidationPeriod::find($prodotto->id_periodo_validazione)->data_fine_concilio;
				if ($ora >= $datainizio && $ora <= $datafine){
					return true;// se si è nel lasso temporale esatto
				} else {
					return false;//se non si è nel lasso temporale esatto
				}
			}


	}

}