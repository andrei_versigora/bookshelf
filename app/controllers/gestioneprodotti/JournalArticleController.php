<?php

namespace GestioneProdotti;

/**
 * 
 */
class JournalArticleController implements ProductManagerControllerImpl
{

	/**
	* Restituisce un array di regole da applicare al prodotto di tipo JournalArticle
	* @return array
	*/
	function getRules()
	{
		$rules=array(
			"titolo_rivista"=>"required|max:50",
			"doi"=> ['regex:/10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+/'],
			"issn"=>"digits:9|regex:/[0-9]-[0-9]/",
			"num_rivista"=>"integer",
			"num_pagina_inizio"=>"integer",
			"num_pagina_fine"=>"integer"
			);

		return $rules;
	}

	/**
	* Restituisce un prodotto nuovo o uno prodotto gia esistente nel database di tipo 
	* JournalArticle
	* @param idProdotto
	* @return JournalArticle
	*/
	function getDetails($idProdotto = null)
	{
		$articolo=new \JournalArticle;

		//controlla se e' stata passata la variabile idProdotto
		if(!empty($idProdotto)){
			$articolo=\JournalArticle::find($idProdotto);
		}

		$articolo->titolo_rivista=\Input::get("titolo_rivista");
		$articolo->doi=\Input::get("doi");
		$articolo->issn=\Input::get("issn");
		$articolo->num_pagina_inizio=\Input::get("num_pagina_inizio");
		$articolo->num_pagina_fine=\Input::get("num_pagina_fine");
		$articolo->num_rivista=\Input::get("num_rivista");
		
		return $articolo;
	}

	/**
	* Restituisce la view per l'inserimento
	* @return View
	*/
	function getInsertView()
	{
		return \View::make('gestioneprodottigui/formprodotti/dettagli-articolo-su-rivista');
	}
	
	/**
	* Restituisce la view per la modifica
	* @return View
	*/
	function getModifyView()
	{
		return \View::make('gestioneprodottigui/formprodotti/dettagli-articolo-su-rivista-modifica');
	}

}