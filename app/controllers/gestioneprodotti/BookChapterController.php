<?php  

namespace GestioneProdotti;

/**
 * 
 */
class BookChapterController implements ProductManagerControllerImpl
{
	/**
	* Restituisce un array di regole da applicare al prodotto di tipo BookChapter
	* @return array
	*/
	function getRules()
	{
		// Le regole che i parametri devono rispettare
		$rules=array(
			"titolo_capitolo"=>"max:50",
			"doi"=> ['regex:/10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+/'],
			"isbn"=>"digits:13|numeric",
			"casa_editrice"=>"max:20",
			"num_pagina_inizio"=>"integer",
			"titolo_libro"=>"required|max:50",
			"num_pagina_fine"=>"integer",
			"data_stesura"=>"date_format:j/n/Y",
			"nome_editori"=>"max:20"
			);

		return $rules;
	}

	/**
	* Restituisce un prodotto nuovo o uno prodotto gia esistente nel database di tipo 
	* BookChapter 
	* @param idProdotto
	* @return BookChapter
	*/
	function getDetails($idprodotto = null)
	{
		$libro=new \BookChapter;

		//controlla se e' stata passata la variabile idProdotto
		if(!empty($idprodotto)){
			$libro=\BookChapter::find($idprodotto);
		}

		$libro->doi=\Input::get('doi');
		$libro->titolo_capitolo=\Input::get('titolo_capitolo');
		$libro->titolo_libro=\Input::get('titolo_libro');
		$libro->isbn=\Input::get('isbn');
		$libro->casa_editrice=\Input::get('casa_editrice');
		$libro->num_pagina_inizio=\Input::get('num_pagina_inizio');
		$libro->num_pagina_fine=\Input::get('num_pagina_fine');
		$libro->data_stesura=\Input::get('data_stesura');
		$libro->nome_editori=\Input::get('nome_editori');
		
		return $libro;	
	}

	/**
	* Restituisce la view per l'inserimento
	* @return View
	*/
	function getInsertView(){
		return \View::make('gestioneprodottigui/formprodotti/dettagli-libro-capitolo');
	}
	
	/**
	* Restituisce la view per la modifica
	* @return View
	*/
	function getModifyView(){
		return \View::make('gestioneprodottigui/formprodotti/dettagli-libro-capitolo-modifica');
	}
}