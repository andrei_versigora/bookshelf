<?php

namespace GestioneProdotti;

interface ProductManagerControllerImpl 
{

	/**
	* Restituisce le regole che gli input relativi ai dettagli del
	* prodotto devono rispettare.
	* @return array
	*/
	public function getRules();

	/**
	* Restituisce una tipologia specifica del ResearchProduct
	* @return ResearchProduct
	*/
	public function getDetails($idProdotto);

	/**
	* Restituisce la view per l'inserimento
	* @return View
	*/
	public function getInsertView();

	/**
	* Restituisce la view per la modifica
	* @return View
	*/
	public function getModifyView();
}