<?php

namespace GestioneProdotti;

class SearchProductController extends \Controller {

	/**
	 * Controller per la ricerca dei prodotti.
	 * 
	 * Request Input:
	 * @param $idAreaScientifica int
	 * @param $tipologia string
	 * @param $autore string
	 * @param $idAutore int
	 * @param $titolo string
	 * @param $DataInizio string
	 * @param $DataFine string
	 *
	 * @return view
	 */
	public function ricercaProdotti()
	{	
		$idAreaScientifica = \Input::get('IDAreaScientifica');
		$tipologia = \Input::get('Tipologia');
		$autore = \Input::get('Autore');
		$idAutore = \Input::get('AutoreId');
		$titolo = \Input::get('Titolo');
		$start_data= \Input::get('DataInizio');
		$end_data= \Input::get('DataFine');

		$query = \ResearchProduct::cerca($idAreaScientifica, $tipologia, $autore, $idAutore, $titolo, $start_data, $end_data);
		
		// Il risulato dev'essere "paginato"
		$prodotti = $query->paginate(30);

		// ATTENZIONE!
		// Converte la lista di id nei rispettivi modelli. Questa operazione potrebbe essere piuttosto costosa.
		//$models = $this->idList2ProductList($prodotti);

		// Decommentare la prossima riga per stampare a schermo il risultato della query.
		//return $prodotti;
		return \View::make('gestioneprodottigui/ricercaprodotti')
			->withInput(\Input::all())
			->with('prodotti', $prodotti);
	}

}