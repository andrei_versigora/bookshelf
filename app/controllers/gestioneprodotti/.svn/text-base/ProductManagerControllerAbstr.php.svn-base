<?php

namespace GestioneProdotti;

/**
*
*/
class ProductManagerControllerAbstr extends \Controller{

	private $impl;

	/**	
	* Costruttore che definisce il tipo di Prodotto su cui andremo a lavorare	
	*/
	public function __construct() 
	{
		$this->beforeFilter(function($tipologia)
        {
			$this->setImplByProductType(\Input::get('tipologia'));
        });
	}

	/**
	* Funzione che inizializza la variabile impl
	*/
	public function setImpl($impl) 
	{
		$this->impl = $impl;
	}

	/**
	* Funzione che determina il tipo specifico del prodotto
	*/
	private function setImplByProductType($type)
	{
		switch ($type) {
			case 'atto_di_congresso':
				$this->setImpl( new ActOfCongressController() );
				break;
			case 'libro_capitolo':
				$this->setImpl( new BookChapterController() );
				break;
			case 'edizione_critica':
				$this->setImpl( new CriticalEditionController() );
				break;
			case 'traduzione':
				$this->setImpl( new TranslationController() );
				break;
			case 'commento':
				$this->setImpl( new ScientificCommentaryController() );
				break;
			case 'altro':
				$this->setImpl( new OtherController() );
				break;
			case 'articolo_su_rivista':
				$this->setImpl( new JournalArticleController() );
				break;
			// Se non trova alcun filtro
			default:
				$this->setImpl( new JournalArticleController() );
				break;
		}
	}

	/**
	* Restituisce le regole per l'inserimento
	* @return array
	*/
	private function getInsertRules()
	{
		$commonrules = $this->commonRules();
		$prules = $this->impl->getRules();
		$rules = array_merge($commonrules, $prules);

		return $rules;
	}

	/**
	* Restituisce le regole per la modifica
	* @return array
	*/
	private function getModifyRules()
	{
		$commonrules = $this->commonRules();
		$prules = $this->impl->getRules();
		$idRule = ['id' => 'required|numeric|exists:prodotto_di_ricerca'];
		$rules = array_merge($commonrules, $prules);
		$rules = array_merge($rules, $idRule);
		return $rules;
	}

	/**
	* Restituisce le regole del prodotto comune
	* @return array
	*/
	public function commonRules()
	{
		$rules=array('titolo_del_prodotto_di_ricerca' => 'required|max:200',
					"data_pubblicazione"=>"date_format:j/n/Y|required");
		return $rules;
	}

	/**
	* Funzionalità che permette di aggiungere un tipo specifico di prodotto, controllando se l'input è corretto
	* e aggiugendo per primo il prodotto generale e subito dopo il prodotto specifico in base alla tipologia	
	* 
	* Request inputs:
	* @param titolo_del_prodotto_di_ricerca
	* @param descrizione
	* 
	* Inoltre riceve come input i parametri specifici della tipologia prodotto di ricerca.
	*/
	public function addProduct()
	{
		$rules = $this->getInsertRules();
		$validator = \Validator::make(\Input::all(),$rules);
		
		if($validator->fails())
		{
			return \Redirect::back()->withInput(\Input::all())->withErrors($validator);
		}

		\DB::transaction(function()
		{
			$prodotto=new \ResearchProduct;

			$prodotto->titolo_del_prodotto_di_ricerca=\Input::get('titolo_del_prodotto_di_ricerca');
			$prodotto->descrizione=\Input::get('descrizione');
			$prodotto->stato_validazione= \ResearchProduct::NON_VALIDATO;
			$prodotto->tipologia_del_prodotto=\Input::get('tipologia');

			// Converti data
			$dataString = \Input::get('data_pubblicazione');
			$data = \DateTime::createFromFormat('d/m/Y', $dataString);
			$prodotto->data_pubblicazione = $data;

			$dettagli=$this->impl->getDetails();

			$prodotto->save();
			$dettagli->id_prodotto_di_ricerca=$prodotto->id;
			$dettagli->save();

			$autore=\Auth::user();
			$autore->prodotti()->save($prodotto,['ha_permessi'=>1]);

			if(!\Input::has('autori')){
				return;
			}

			$idAutori=\Input::get('autori');
		
			foreach ($idAutori as $id) {
				if ($id == \Auth::user()->id) {
					continue;
				}

				$realizza = new \realization;
				$realizza->id_prodotto_di_ricerca = $prodotto->id;
				$realizza->id_ricercatore = $id;
				$realizza->save();
			}

		});

		return \Redirect::route('prodotti')->withInput(\Input::all());
	}

	/**
	* Funzione che si occupa della modifica del prodotto di ricerca generale e specifico
	* tenere in considerazione la condizione che nel caso il prodotto è validato non può 
	* essere modificato
	*/
	public function modifyProduct()
	{	
		$idProdotto=\Input::get("id");
		$rules = ['id' => 'required|numeric|exists:prodotto_di_ricerca'];
		$validator=\Validator::make(\Input::all(), $rules);
		if ($validator->fails()) {
			// Not found
			return "prodotto non trovato";//\App::abort(404);
		}

		if ((\Auth::user()->tipologia == \Utente::RICERCATORE) && (\ResearchProduct::verificaPermessi(\Auth::user()->id,$idProdotto))) {
			
			// Id del prodotto da modificare
			$idProdotto=\Input::get("id");
			
			$prodotto=\ResearchProduct::find($idProdotto);
			$this->setImplByProductType($prodotto->tipologia_del_prodotto);

			$rules=$this->getModifyRules();
			$validator=\Validator::make(\Input::all(),$rules);

			if($validator->fails())
			{
				return \Redirect::back()->withInput(\Input::all())->withErrors($validator);
			}
			
			\DB::transaction(function($prodotto) use ($prodotto)
			{	
				

				$prodotto->titolo_del_prodotto_di_ricerca=\Input::get('titolo_del_prodotto_di_ricerca');
				$prodotto->descrizione=\Input::get('descrizione');
				$prodotto->stato_validazione= \ResearchProduct::NON_VALIDATO;
				$prodotto->tipologia_del_prodotto=\Input::get('tipologia');

				// Converti data
				$dataString = \Input::get('data_pubblicazione');
				$data = \DateTime::createFromFormat('d/m/Y', $dataString);
				$prodotto->data_pubblicazione = $data;

				$dettagli=$this->impl->getDetails($prodotto->id);

				$prodotto->save();
				$dettagli->save();

				$prodotto->ricercatori()->detach();

				$autore=\Auth::user();
				$autore->prodotti()->save($prodotto,['ha_permessi'=>1]);

				if(!\Input::has('autori')){
					return;
				}

				$idAutori=\Input::get('autori');
			
				foreach ($idAutori as $id){
					if ($id == \Auth::user()->id) {
						continue;
					}
					
					$realizza = new \realization;
					$realizza->id_prodotto_di_ricerca = $prodotto->id;
					$realizza->id_ricercatore = $id;
					$realizza->save();
				}

			});

			return \Redirect::route('prodotti')->withInput(\Input::all());
		}	
		else{
			return "non hai permessi di modifica";
		}


			//gestione dei permessi
		if (\Auth::user()->tipologia == \Utente::AMMINISTRATORE) {
			$permessi=\Input::get('permessiAutore');
			$prodotto=\ResearchProduct::find(\Input::get("id"));
			
			foreach ($permessi as $idAutori) {
				$realizzazione=$prodotto->ricercatori()->where('id_ricercatore','=',$idAutori)->first();
				if($realizzazione->pivot->ha_permessi==1){
					$realizzazione->pivot->ha_permessi = 0;
					$realizzazione->pivot->save();
				}
				else{
					$realizzazione->pivot->ha_permessi = 1;
					$realizzazione->pivot->save();
				}
			}
			
			return "i permessi sono stati modificati";
		}
	}

	/**
	* Restituisce la view che permette l'inserimento di un nuovo prodotto
	* @return View
	*/
	public function getInsertView()
	{	
		return $this->impl->getInsertView();	
	}

	/**
	* Restituisce la view che permette l'inserimento o modifica di un nuovo prodotto
	* @return View
	*/
	public function getModifyView()
	{	
		$rules = ['id' => 'required|numeric|exists:prodotto_di_ricerca'];
		$validator=\Validator::make(\Input::all(), $rules);
		if ($validator->fails()) {
			return \Redirect::route('prodotti')->withInput(\Input::all())->withErrors($validator);
		}

		$idProdotto = \Input::get('id');
		$prodotto = \ResearchProduct::find($idProdotto);
		$this->setImplByProductType($prodotto->tipologia_del_prodotto);

		return $this->impl->getModifyView()->with('prodotto', $prodotto);	
	}

}
