<?php  
namespace GestioneProdotti;
/**
* 
*/
class ActOfCongressController implements ProductManagerControllerImpl
{
	/**
	* Restituisce un array di regole da applicare al prodotto di tipo ActOfCongress
	* @return array
	*/
	function getRules()
	{
		// Le regole che i parametri devono rispettare
		$rules=array(
			"doi"=> ['regex:/10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+/'],
			"isbn"=>"numeric|digits:13",
			"num_pagina_inizio"=>"integer",
			"num_pagina_fine"=>"integer",
			"nome_convegno"=>"max:30",
			"editore_convegno"=>"alpha|max:30",
			"luogo_convegno"=>"max:30"
			);

		return $rules;
	}
	/**
	* Restituisce un prodotto nuovo o uno prodotto gia esistente nel database di tipo
	* ActOfCongress 
	* @param idProdotto
	* @return ActOfCongress
	*/
	function getDetails($idProdotto = null)
	{
		$atto = new \ActOfCongress;

		//controlla se e' stata passata la variabile idProdotto
		if (!empty($idProdotto)) {
			$atto = \ActOfCongress::find($idProdotto);
		}

		$atto->doi = \Input::get("doi");
		$atto->isbn = \Input::get("isbn");
		$atto->num_pagina_inizio = \Input::get("num_pagina_inizio");
		$atto->num_pagina_fine = \Input::get("num_pagina_fine");
		$atto->nome_convegno = \Input::get("nome_convegno");
		$atto->editore_convegno = \Input::get("editore_convegno");
		$atto->luogo_convegno = \Input::get("luogo_convegno");
		$atto->data_convegno = \Input::get("data_convegno");

		return $atto;
	}
	
	/**
	* Restituisce la view per l'inserimento
	* @return View
	*/
	function getInsertView(){
		return \View::make('gestioneprodottigui/formprodotti/dettagli-atto-di-congresso');
	}
	
	/**
	* Restituisce la view per la modifica
	* @return View
	*/
	function getModifyView(){
		return \View::make('gestioneprodottigui/formprodotti/dettagli-atto-di-congresso-modifica');
	}
}
