<?php

namespace GestioneProdotti;

class ProductManager extends \Controller {

	/**
	* Visualizza tutti i prodotti dell'utente attualmente loggato.
	* @return View
	*/
	public function visualizzaProdotti()
	{	
		$prodotti = \Auth::user()->prodotti()->paginate(30);
	    return \View::make('gestioneprodottigui/visualizzaProdotti')
	    	->with('prodotti', $prodotti)
	    	->with('idProdotti', $prodotti);
	}

	/**
	* Visualizza il prodotto che l'utente ha selezionato.
	* @return View
	*/
	public function visualizzaProdotto()
	{	
		// Le regole che i parametri devono rispettare
		$rules = ['id' => 'required|numeric|exists:prodotto_di_ricerca'];

		// Verifica che i parametri siano conformi alle regole specificate...
	    $validator = \Validator::make(\Input::all(), $rules);

	    // ...se non sono conformi
	    if ($validator->fails())
	    {	
	    	return \Redirect::route('prodotti')->withInput(\Input::all())->withErrors($validator);
	    }

	    $id = \Input::get("id");
	    $prodotto = \ResearchProduct::find($id);
	    return \View::make('gestioneprodottigui/visualizzaprodotto')
	    	->with('prodotto', $prodotto);
	}

	/**
	* Effettua l'eliminazione del prodotto, la relazione a cui è legata e il prodotto specifico
	*/
	public function deleteProduct()
	{	
		// Verifica che il prodotto esista
		$rules = [
			// Aggiungere il requisito che il prodotto esista nel DB
			'id' => 'required|numeric|exists:prodotto_di_ricerca'
		];

		$validator = \Validator::make(\Input::all(), $rules);
		
		if($validator->fails())
		{
			return \GuiUtils::error('Il prodotto selezionato non è presente nel database');
		}

		$id = \Input::get('id');

		$prodotto = \ResearchProduct::find($id);
		if (\Auth::user()->tipologia == \Utente::AMMINISTRATORE) {
			$prodotto->delete();
			return \View::make('assert.success');
			//return \Redirect::route('prodotti')->withInput(\Input::all());
		}

		// Verifica che il prodotto appartenga all'utente loggato e che questi
		// abbia permessi di modifica.
		$ricercatore = $prodotto->ricercatori()
			->where('id_ricercatore', '=', \Auth::user()->id)
			->where('ha_permessi', '=', '1')->first();

		if ((!empty($ricercatore)) && ($prodotto->stato_validazione == \ResearchProduct::NON_VALIDATO)) {
			$prodotto->delete();
			return \GuiUtils::success('Il prodotto è stato eliminato correttamente!');
			// return \Redirect::route('prodotti')->withInput(\Input::all());
		}

		return \GuiUtils::error('Non puoi eliminare questo prodotto');
	}

	/**
	* Restituisce la view per la gestione dei permessi
	* @return View
	*/
	public function gestionePermessiView()
	{	
		$idProdotto=\Input::get("id");
		$prodotto=\ResearchProduct::find($idProdotto);
		return \View::make('gestioneprodottigui/formprodotti/gestionepermessi')->with("prodotto",$prodotto);

	}
}