<?php  

namespace GestioneProdotti;

/**
*
*/
class CriticalEditionTranslationAndScientificCommentaryController implements ProductManagerControllerImpl
{

	/**
	* Restituisce un array di regole da applicare al prodotto di tipo
	* CriticalEditionTranslationAndScientificCommentary
	* @return array
	*/
	function getRules()
	{
		// Le regole che i parametri devono rispettare
		$rules=array(
			"lingua"=>"alpha|max:20",
			"codici"=>"max:25"
			);

		return $rules;
	}

	/**
	* Restituisce un prodotto nuovo o uno prodotto gia esistente nel database di tipo 
	* CriticalEditionTranslationAndScientificCommentary 
	* @param idProdotto
	* @return CriticalEditionTranslationAndScientificCommentary
	*/
	function getDetails($idProdotto = null)
	{
		$edit=new \CriticalEditionTranslationAndScientificCommentary;

		//controlla se e' stata passata la variabile idProdotto
		if(!empty($idProdotto)){
			$edit=\CriticalEditionTranslationAndScientificCommentary::find($idProdotto);
		}

		
		$edit->lingua=\Input::get('lingua');
		$edit->codici=\Input::get('codici');
		
		return $edit;
	}

	/**
	* Restituisce la view per l'inserimento
	* @return View
	*/
	function getInsertView()
	{
		return \View::make('gestioneprodottigui/formprodotti/dettagli-commenti-traduzioni-edizioni');
	}
	
	/**
	* Restituisce la view per la modifica
	* @return View
	*/
	function getModifyView()
	{
		return \View::make('gestioneprodottigui/formprodotti/dettagli-commenti-traduzioni-edizioni-modifica');
	}

}
