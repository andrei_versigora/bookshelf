<?php  
namespace GestioneProdotti;

/**
 * 
 */
class OtherController implements ProductManagerControllerImpl
{
	/**
	* Restituisce un array di regole da applicare al prodotto di tipo ActOfCongress
	* @return array
	*/
	function getRules()
	{
		// Le regole che i parametri devono rispettare
		$rules=array(
				"tipologia_altro"=>"required"
		);

		return $rules;
	}
	/**
	* Restituisce un prodotto nuovo o uno prodotto gia esistente nel database di tipo Other 
	* @return Other
	*/
	function getDetails($idProdotto = null)
	{
		$altro=new \Other;

		//controlla se e' stata passata la variabile idProdotto
		if(!empty($idProdotto)){
			$altro=\Other::find($idProdotto);
		}

		$altro->tipologia_altro=\Input::get("tipologia_altro");

		return $altro;
	}

	/**
	* Restituisce la view per l'inserimento
	* @return View
	*/
	function getInsertView()
	{
		return \View::make('gestioneprodottigui/formprodotti/dettagli-altro');
	}
	
	/**
	* Restituisce la view per la modifica
	* @return View
	*/
	function getModifyView(){
		return \View::make('gestioneprodottigui/formprodotti/dettagli-altro-modifica');
	}
}