<?php

namespace GestioneValutazione;

/**
* 
*/
class ProductValutationController extends \Controller
{	
	/**
	* Restituisce la lista dei prodotti da valutare dalla tabella lista_prodotti_per_valutazione
	*
	* @return query
	*/
	private function listaProdotti()
	{
		//restituisce il periodo attuale
		$periodoAttuale=\EvalutationPeriod::caricaPeriodoValutazioneAttuale();

		$lista=\EvalutationProduct::listaProdottiDaValutare($periodoAttuale->id);

		return $lista;
	}

	/**
	 * Visualizza la lista dei prodotti da valutare diversa per ogni tipologia di utente. in base al campo posizione.
	 *
	 * @return View
	 */
	public function listaProdottiDaValutare()
	{
		if (\Auth::user()->tipologia == \Utente::DIR_DISCIPLINARE) 
		{

			// restituisce l'area di appartenenza del responsabile logato
			$idAreaScientifica=\Auth::user()->id_area_scientifica;

			
			$lista=$this->listaProdotti()
				->where('id_area_scientifica','=',$idAreaScientifica)->get();

			//return $lista;
			
			return \View::make('gestionevalutazione/listaProdottiResponsabileArea')->with('prodotti', $lista);
		}

		if (\Auth::user()->tipologia == \Utente::RES_ATENEO) 
		{

			$lista=\EvalutationProduct::query()
				->join('utente', 'lista_prodotti_per_valutazione.id_ricercatore', '=', 'utente.id')->select('lista_prodotti_per_valutazione.id','utente.nome','utente.cognome')->get();

			return \View::make('gestionevalutazione/listaProdottiResponsabileAteneo')->with('ricercatori', $lista);
		}
	}

	private function controllaConflitti()
	{
		$idAreaScientifica=\Auth::user()->id_area_scientifica;
		
		//lista di prodotti da valutare per una certa area scientifica
		$lista=\Submit::all()->lists('id_prodotto_di_ricerca');

		$cnt=count($lista);
		$listaConflitti=array();

		//si fa il controllo su id dei prodotti per trovare se ci sono prodotti con gli stessi id,se SI, l'id si aggiunge in un array 
		for ($i=0; $i < $cnt-1; $i++){
			for ($j=$i+1; $j < $cnt; $j++){ 
				if ($lista[$i]==$lista[$j]) 
				{
					$listaConflitti[]=$lista[$i];
				}
			}
		}

		return $listaConflitti;
	}

	/**
	* Restituisce la lista dei prodotti da valutare che si trovano in contesa
	*
	* @return View
	*/
	public function visualizzaProdottiConflitti()
	{
		$idProdotti=$this->controllaConflitti();

		if (empty($idProdotti)) {
			return \Redirect::route('lista-prodotti');
		}
		
		if (\Auth::user()->tipologia == \Utente::DIR_DISCIPLINARE)
		{
			$idAreaScientifica=\Auth::user()->id_area_scientifica;

			$prodottiInConflitto=$this->listaProdotti()
				->where('id_area_scientifica','=',$idAreaScientifica)
				->whereIn('id_prodotto_di_ricerca', $idProdotti)
				->orderBy('id_prodotto_di_ricerca')->get();
		}

		if (\Auth::user()->tipologia == \Utente::RES_ATENEO) 
		{
			$prodottiInConflitto=$this->listaProdotti()
				->whereIn('id_prodotto_di_ricerca', $idProdotti)
				->orderBy('id_prodotto_di_ricerca')->get();
		}
		return \View::make('gestionevalutazione/visualizzaConflitti')->with('prodotti', $prodottiInConflitto);
	}

	/**
	* Restituisce la lista dei prodotti da valutare di un particolare ricercatore
	*
	* @param string id_ricercatore
	*
	* @return View
	*/
	public function listaProdottiDaValutareXRicercatore()
	{	
		$idRicercatore=\Input::get('id_ricercatore');
			
		// restituisce l'area di appartenenza del responsabile logato
		$idAreaScientifica=\Auth::user()->id_area_scientifica;

		$lista=$this->listaProdotti()
			->where('id_area_scientifica','=',$idAreaScientifica)
			->where('id_ricercatore','=',$idRicercatore)->get();

		return \View::make('gestionevalutazione/listaProdottiXRicercatore')->with('prodotti', $lista);
		
	}

	/**
	* Funzione che elimina un prodotto dalla lista dei prodotti da valutare 
	* (tabella inserimento)
	*
	* @param string id_prodotto_di_ricerca
	* @param string id_lista_prodotti_per_valutazione
	*
	* @return View
	*/
	public function eliminaProdotto()
	{	
		// Verifica che il prodotto esista
		$rules = [
			// Aggiungere il requisito che il prodotto esista nel DB
			'id_prodotto_di_ricerca' => 'required|numeric|exists:prodotto_di_ricerca,id'
		];

		$validator = \Validator::make(\Input::all(), $rules);
		
		if($validator->fails())
		{
			return \Redirect::route('visualizza-conflitti')->with('error', 'Il prodotto selezionato non è presente nel database');
		}
		$idProdotto=\Input::get('id_prodotto_di_ricerca');
		
		$idListaProdotti=\Input::get('id_lista_prodotti_per_valutazione');

		$prodotto=\Submit::where('id_prodotto_di_ricerca','=',$idProdotto)
	    	->where('id_lista_prodotti_per_valutazione','=',$idListaProdotti);

		$prodotto->delete();

		return \Redirect::route('visualizza-conflitti');
		
	}
	public function invioAlVQR()
	{
		$idListaProdotti=\Input::get('id');
		
		$listaProdotti=\EvalutationProduct::find($idListaProdotti);

		$listaProdotti->delete();

		return \Redirect::route('lista-prodotti');
	}
}