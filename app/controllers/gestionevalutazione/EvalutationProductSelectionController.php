<?php

namespace GestioneValutazione;

class EvalutationProductSelectionController extends \Controller 
{

	private static $PRODOTTI_DA_VALUTARE = 'prodotti_da_valutare';

	/**
	 * Visualizza la lista dei prodotti che possono essere selezionati per la valutazione.
	 *
	 * @return View
	 */
	public function listaProdottiDaSottomettere()
	{
		$prodotti = \Auth::user()->prodotti()->get();
		
		return \View::make('gestionevalutazione/listaProdotti')->with('prodotti', $prodotti);
	}
	
	public function submit()
	{
		
		$lista=\Auth::user()->evalutationSubmitList();
		if(! $lista==null)
			{
				return \GuiUtils::error("Hai già una lista.");
			}

		$lista = new \EvalutationProduct;
				$lista->id_ricercatore = \Auth::user()->id; 
				$lista->id_periodo_valutazione=\EvalutationPeriod::first()->id;
				$lista->save();

		$count=1;
		$sel='P'.$count;
		$array=array();
		while(($input=\Input::get($sel))!=null)
		{
			$sub=new \Submit;
			$sub->posizione=$count;
			$sub->id_lista_prodotti_per_valutazione=$lista->id;
			$sub->id_prodotto_di_ricerca=$input;
			$sub->save();


			$count++;
			$sel="P".$count;
		}

		return \GuiUtils::success("La tua lista dei prodotti è stata correttamente sottomessa per la valutazione.");

		return "OK";
		//$query=\EvalutationProduct::where('id_ricercatore','=','1')->first();

		//return var_dump($query->prodotti()->get());

	}

	
}