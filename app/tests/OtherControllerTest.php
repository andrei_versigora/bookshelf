<?php

use GestioneProdotti\OtherController;

class OtherControllerTest extends TestCase {
	  
	/**
	 * Verifica che il metodo restituisca le regole
	 */
	public function testGetRules()
	{
	 	$controllers = new OtherController; 
	 	$rules = $controllers->getRules();
		$this->assertNotEmpty($rules, 'Non sono state restituite le regole');
	}
	
	/**
	 * Verifica che venga restituito il prodotto
	 */
	public function testGetDetails() {
	    $controllers = new OtherController; 
		$prod = $controllers->getDetails();
		$this->assertNotEmpty($prod, 'Non e\' stato restituito alcun prodotto');
	}	
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetInsertView()
	{
	 	$controller = new OtherController;
		$response = $controller->getInsertView();
		$this->assertNotEmpty('View non presente');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetModifyView()
	{
	 	$controller = new OtherController;
		$response = $controller->getModifyView();
		$this->assertNotEmpty('View non presente');
	}
}