<?php

// Controllata
	
class ActOfCongressTest extends TestCase {
	
	/**
	 * Verifica che il metodo find trovi un atto di congresso quando presente
	 */
	public function testFindPresent()
	{
		$id = 5;
		$act = \ActOfCongress::find($id);
		$this->assertNotEmpty($act, 'Non e\' stato trovato l\'atto di congresso');
	}

	/**
	 * Verifica che il metodo find trovi l'atto di congresso quando non presente
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$act = \ActOfCongress::find($id);
		$this->assertEmpty($act, 'E\' stato trovato l\'atto di congresso');
	}
	
}
