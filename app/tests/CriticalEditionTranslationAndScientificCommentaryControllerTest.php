<?php

use GestioneProdotti\CriticalEditionTranslationAndScientificCommentaryController;

class CriticalEditionTranslationAndScientificCommentaryControllerTest extends TestCase {
	  
	/**
	 * Verifica che il metodo restituisca le regole
	 */
	public function testGetRules()
	{
	 	$controller = new CriticalEditionTranslationAndScientificCommentaryController;
	 	$rules = $controller->getRules();
		$this->assertNotEmpty($rules, 'Non sono state restituite le regole');
	}
	
	/**
	 * Verifica che venga restituito il prodotto
	 */
	public function testGetDetails() {
		$controller = new CriticalEditionTranslationAndScientificCommentaryController;
//		$id = 1;
		$prod = $controller->getDetails();
		$this->assertNotEmpty($prod, 'Non e\' stato restituito alcun prodotto');
	}	
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetInsertView()
	{
		$controller = new CriticalEditionTranslationAndScientificCommentaryController;
		$view = $controller->getInsertView();
		$this->assertNotEmpty($view, 'Non e\' stata restituita alcuna view');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetModifyView()
	{
		$controller = new CriticalEditionTranslationAndScientificCommentaryController;
		$view = $controller->getModifyView();
		$this->assertNotEmpty($view, 'Non e\' stata restituita alcuna view');
	}
}

?>