<?php

use GestioneProdotti\PatentController;

abstract class PatentControllerTest extends TestCase {
	  
	/**
	 * Verifica che il metodo restituisca le regole
	 */
	public function testGetRules()
	{
	 	$controllers = new PatentController; 
	 	$rules = $controllers->getRules();
		$this->assertNotEmpty($rules, 'Non sono state restituite le regole');
	}
	
	/**
	 * Verifica che venga restituita la view nel caso di un errore nell'inserimento di un prodotto
	 */
	public function testGetInvalidView(){
		   $controllers = new PatentController;
		   $view = $controllers->getInvalidView();
		   $this->assertNotEmpty($view, 'Non e\' stata restituita la view');
	}
	
	/**
	 * Verifica che venga restituito il prodotto
	 */
	public function testGetDetails() {
	    $controllers = new PatentController; 
		$prod = $controllers->getDetails();
		$this->assertNotEmpty($prod, 'Non e\' stato restituito alcun prodotto');
	}	
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetValidView()
	{
	 	$controller = new PatentController;
		$response = $controller->getValidView();
		$this->assertNotEmpty('View non presente');
	}
	
}