<?php

use GestioneProdotti\ActOfCongressController;

class ActOfCongressControllerTest extends TestCase {
	
    /**
	 * Verifica che il metodo restituisca le regole
	 */
	public function testGetRules()
	{
		$controller = new ActOfCongressController;
	 	$rules = $controller->getRules();
		$this->assertNotEmpty($rules, 'Non sono state restituite le regole');
	}
	
	/**
	 * Verifica che il metodo restituisca l'atto di congresso
	 */
	public function testGetDetails() {
		   $controller = new ActOfCongressController;
		   $act = $controller->getDetails();
		   $this->assertNotEmpty($act, 'Non e\' presente l\'atto di congresso');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetInsertView()
	{
	 	$controller = new ActOfCongressController;
		$response = $controller->getInsertView();
		$this->assertNotempty('View non presente');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetModifyView()
	{
		$controller = new ActOfCongressController;
		$response = $controller->getModifyView();
		$this->assertNotEmpty('View non presente');
	}

}
?>