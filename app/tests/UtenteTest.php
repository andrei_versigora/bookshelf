<?php

// Controllata

class UtenteTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi un utente quando presente
	 */
	public function testFindPresent()
	{
		$id = 1;
		$utente = \Utente::find($id);
		$this->assertNotEmpty($utente, 'Non è stato possibile trovare l\'utente');
	}

	/**
	 * Verifica che il metodo find trovi un utente quando non presente
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$utente = \Utente::find($id);
		$this->assertEmpty($utente, 'E\' stato trovato l\'utente con id = -1');
	}

	/**
	 * Verifica che il metodo prodotti trovi i prodotti di un utente
	 */
	public function testProdotti()
	{
	 	$id = 1;
	 	$utente = \Utente::find($id);
		$prod = $utente->prodotti();
		$this->assertNotEmpty($prod, 'Non sono presenti prodotti');
	}
	
	/**
	 * Verifica che il metodo prodottiConPermessi restituisca i prodotti corretti
	 */
	public function testProdottiConPermessi(){
		$id = 1;
		$utente = \Utente::find($id);
		$prod = $utente->prodottiConPermessi();
		$this->assertNotEmpty($prod, 'L\'utente non ha i permessi per alcun prodotto');
	}

	/**
	 * Verifica che il metodo ricercatori restituisca i ricercatori
	 */
	public function testRicercatori(){
		$ricercatori = \Utente::ricercatori();
		$this->assertNotEmpty($ricercatori, 'Non sono presenti ricercatori');
	}
	
	/**
	 * Verifica che venga restituito l'identificatore unico per l'utente
	 */
	public function testGetAuthIdentifier() {
		$id = 1;
		$utenti = \Utente::find($id);
		$user = $utenti->getAuthIdentifier();
		$this->assertNotEmpty($user, 'L\'utente non ha ID');
	}
	
	/**
	 * Verifica che venga restituita la password per l'utente
	 */
	public function testGetAuthPassword() {
		$id = 1;
		$utenti = \Utente::find($id);
		$user = $utenti->getAuthPassword();
		$this->assertNotEmpty($user, 'L\'utente non ha password');	
	}
	
	/**
	 * Verifica che non venga restituita una password per un utente esterno 
	 */
	public function test2GetAuthPassword() {
		$id = 48;
		$utenti = \Utente::find($id);
		$user = $utenti->getAuthPassword();
		$this->assertEmpty($user, 'L\'utente ha password');	
	}
}

?>