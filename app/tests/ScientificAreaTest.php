<?php
	
class ScientificAreaTest extends TestCase {
	

	public function testFindPresent()
	{
		$id = 1;
		$act = \ScientificArea::find($id);
		$this->assertNotEmpty($act, 'Errore assertNotEmpty');
	}


	public function testFindNotPresent()
	{
		$id = -1;
		$act = \ScientificArea::find($id);
		$this->assertEmpty($act, 'Errore asserEmpty');
	}
	/*
	public function testResearchers(){
		$id = 1;
		$utente = \ScientificArea::find($id);
		$ric = $utente->researchers();
		$this->assertNotEmpty($ric, 'Non e\' stato trovato il ricercatore');
	}*/
}
?>