<?php

class EvalutationPeriodTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindPresent()
	{
		$id = 1;
		$ectc = \EvalutationPeriod::find($id);
		$this->assertNotEmpty($ectc, 'Non e\' stato trovato');
	}

	/**
	 * Verifica che il metodo find non trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$ectc = \EvalutationPeriod::find($id);
		$this->assertEmpty($ectc, 'E\' stato trovato');
	}
	/*
	public function testCaricaPeriodoValutazioneAttuale() {
		   $per = EvalutationPeriod::caricaPeriodoValutazioneAttuale();
		   $this->assertNotEmpty($per, 'Non e\' stato caricato alcun periodo per la valutazione');
	}*/
}

?>