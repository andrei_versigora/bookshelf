<?php

// Controllata

class OtherTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi altro
	 */
	public function testFindPresent()
	{
		$id = 13;
		$altro = \Other::find($id);
		$this->assertNotEmpty($altro, 'Non e\' stato trovato');
	}

	/**
	 * Verifica che il metodo find non trovi altro
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$altro = \Other::find($id);
		$this->assertEmpty($altro, 'E\' stato trovato');
	}

}