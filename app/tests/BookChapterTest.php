<?php

// Controllata

class BookChapterTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi un capitolo del libro
	 */
	public function testFindPresent()
	{
		$id = 10;
		$act = \BookChapter::find($id);
		$this->assertNotEmpty($act, 'Non e\' stato trovato il capitolo del libro');
	}

	/**
	 * Verifica che il metodo find non trovi un capitolo del libro
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$act = \BookChapter::find($id);
		$this->assertEmpty($act, 'E\' stato trovato il capitolo del libro');
	}
	  
}