<?php

// Controllata

class ResearchProductTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi altro
	 */
	public function testFindPresent()
	{
		$id = 1;
		$prod = \ResearchProduct::find($id);
		$this->assertNotEmpty($prod, 'Non e\' stato trovato');
	}

	/**
	 * Verifica che il metodo find non trovi altro
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$prod = \ResearchProduct::find($id);
		$this->assertEmpty($prod, 'E\' stato trovato');
	}

	/**
	 * Verifica che il ricercatore abbia i permessi
	 */
	public function testVerificaPermessi() {
		$id_ric = 3;
		$id_prod = 1;
		$bool = \ResearchProduct::verificaPermessi($id_ric, $id_prod);
		$this->assertTrue($bool, 'Il ricercatore non ha i permessi');
	}
	
    /**
	 * Verifica che il ricercatore abbia i permessi, ma non trova il prodotto
	 */
	public function testVerificaPermessiNoProdotto() {
		$id_ric = 3;
		$id_prod = -1;
		$bool = \ResearchProduct::verificaPermessi($id_ric, $id_prod);
		$this->assertFalse($bool, 'Il sistema ha trovato il prodotto');
	}
	
	/**
	 * Verifica che il metodo ricercatori restituisca i ricercatori
	 */
	public function testRicercatori(){
		$id = 1;
		$prod = \ResearchProduct::find($id);
		$ricercatori = $prod->ricercatori();
		$this->assertNotEmpty($ricercatori, 'Non sono presenti ricercatori');
	}
	
	/**
	 * Verifica che il metodo dettagli restituisca i dettagli del prodotto di ricerca
	 */
	public function testDettagli() {
		$id = 1;
		$prod = \ResearchProduct::find($id);
		$var = $prod->dettagli();
		$this->assertNotEmpty($var, 'Non sono presenti dettagli');
	}
	
	/**
	 * Verifica (nel nostro caso) che l'autore sia presente 
	 */
	public function testCerca() {
		$user = \ResearchProduct::cerca(null, null, "Carlo Blundo", null, null, null, null);
		$this->assertNotEmpty($user, 'Non e\' presente il ricercatore richiesto');
	}
}