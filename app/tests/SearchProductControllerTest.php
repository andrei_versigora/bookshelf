<?php

use GestioneProdotti\SearchProductController;

class SearchProductControllerTest extends TestCase {
	
	public function testRicercaProdotti(){
		$controller = new SearchProductController;
		$view = $controller->ricercaProdotti();
		$this->assertNotEmpty($view, 'Non e\' stata restituita view');
	}

}