<?php

use GestioneProdotti\BookChapterController;

class BookChapterControllerTest extends TestCase {

	/*
	 * Verifica che il metodo restituisca le regole
	 */
	public function testGetRules()
	{
		$controller = new BookChapterController;
	 	$rules = $controller->getRules();
		$this->assertNotEmpty($rules, 'Non sono state restituite le rules');
	}
	
	/*
	 * Verifica che il metodo restituisca il capitolo del libro
	 */
	public function testGetDetails()
	{
		$controller = new BookChapterController;
	 	$rules = $controller->getDetails();
		$this->assertNotEmpty($rules, 'Non e\' stato restituito alcun capitolo');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetInsertView()
	{
	 	$controller = new BookChapterController;
		$response = $controller->getInsertView();
		$this->assertNotempty('View non presente');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetModifyView()
	{
		$controller = new BookChapterController;
		$response = $controller->getModifyView();
		$this->assertNotEmpty('View non presente');
	}
}