<?php

// Controllata

class CriticalEditionTranslationAndScientificCommentaryTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindPresent()
	{
		$id = 18;
		$ectc = \CriticalEditionTranslationAndScientificCommentary::find($id);
		$this->assertNotEmpty($ectc, 'Non e\' stato trovato');
	}

	/**
	 * Verifica che il metodo find non trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$ectc = \CriticalEditionTranslationAndScientificCommentary::find($id);
		$this->assertEmpty($ectc, 'E\' stato trovato');
	}
	  
}