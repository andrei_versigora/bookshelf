<?php

// Controllata

class EvalutationProductTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindPresent()
	{
		// Nella base di dati non è presente
		$id = 1;
		$lisProd = \EvalutationProduct::find($id);
		$this->assertNotEmpty($lisProd, 'Non e\' stato trovato');
	}

	/**
	 * Verifica che il metodo find non trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$lisProd = \EvalutationProduct::find($id);
		$this->assertEmpty($lisProd, 'E\' stato trovato');
	}
	 
	/**
	 * Verifica che il metodo prodotto restituisca il prodotto 
	 */
	public function testProdotti() {
		   $controller = new EvalutationProduct;
		$prod = $controller->prodotti();
		$this->assertNotEmpty($prod, 'Non e\' stato trovato il prodotto');	
	}
	
	public function testRicercatore() {
		   $controller = new EvalutationProduct;
		$prod = $controller->ricercatore();
		$this->assertNotEmpty($prod, 'Non e\' stato trovato il ricercatore');	
	}
	
	public function testPeriodo() {
		   $controller = new EvalutationProduct;
		$prod = $controller->periodo();
		$this->assertNotEmpty($prod, 'Non e\' stato restituito il periodo di valutazione');	
	}
}