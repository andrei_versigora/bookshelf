<?php

use GestioneValutazione\ManagementValuationPeriod;

class ManagementValuationPeriodTest extends TestCase {

	/**
	 * Verifica che venga visualizzata la pagina di impostazione del periodo di valutazione
	 */
	public function testSetValuationPeriodForm()
	{
		$controller = new ManagementValuationPeriod;
		$view = $controller->setValuationPeriodForm();
		$this->assertNotEmpty($view, 'Non e\' presente view');
	}

	public function testDisplaysValutationPeriod()
	{
		$controller = new ManagementValuationPeriod;
		$view = $controller->setValuationPeriodForm();
		$this->assertNotEmpty($view, 'Non e\' presente view');
	}
	
}
?>