<?php

// Controllata

class JournalArticleTest extends TestCase {

	/**
	 * Verifica che il metodo find trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindPresent()
	{
		$id = 1;
		$articolo = \JournalArticle::find($id);
		$this->assertNotEmpty($articolo, 'Non e\' stato trovato');
	}

	/**
	 * Verifica che il metodo find non trovi un'edizione critica, una traduzione oppure un commento
	 */
	public function testFindNotPresent()
	{
		$id = -1;
		$articolo = \JournalArticle::find($id);
		$this->assertEmpty($articolo, 'E\' stato trovato');
	}
	
	
}

?>