<?php

use GestioneUtenti\UserManagementController;

class UserManagementControllerTest extends TestCase {
	
	public function testVisualizzaUtenti(){
		   $controller = new UserManagementController;
		$view = $controller->VisualizzaUtenti();
		$this->assertNotEmpty($view, 'Non e\' stato trovato alcun utente');
	}
	
	public function testVisualizzaLoginPage(){
		   $controller = new UserManagementController;
		$view = $controller->visualizzaLoginPage();
		$this->assertNotEmpty($view, 'Errore nel login');
	}

	public function testLogin(){
		$controller = new UserManagementController;
		$view = $controller->login();
		$this->assertNotEmpty($view, 'Errore login');
	}
	
	public function testLogout(){
		   $controller = new UserManagementController;
		$view = $controller->logout();
		$this->assertNotEmpty($view, 'Errore logout');
	}
	
	public function testInserisciUtente(){
	$controller = new UserManagementController;
		$view = $controller->InserisciUtente();
		$this->assertNotEmpty($view, 'Errore nell\'inserimento');
	}
	
	public function testGetInsView(){
	$controller = new UserManagementController;
		$view = $controller->GetInsView();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testGetSelView(){
	$controller = new UserManagementController;
		$view = $controller->GetSelView();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testModificaUtente(){
	$controller = new UserManagementController;
		$view = $controller->ModificaUtente();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testGetModView(){
	$controller = new UserManagementController;
		$view = $controller->GetModView();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testRicercaUtenti(){
	$controller = new UserManagementController;
		$view = $controller->RicercaUtenti();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testGetDelView(){
	$controller = new UserManagementController;
		$view = $controller->GetDelView();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testGetDelUserView(){
	$controller = new UserManagementController;
		$view = $controller->GetDelUserView();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
	
	public function testRimuoviUtente(){
	$controller = new UserManagementController;
		$view = $controller->RimuoviUtente();
		$this->assertNotEmpty($view, 'Errore nella view');
	}
}
?>