<?php

use GestioneProdotti\JournalArticleController;

class JournalArticleControllerTest extends TestCase {
	  
	/**
	 * Verifica che il metodo restituisca le regole
	 */
	public function testGetRules()
	{
	 	$controllers = new JournalArticleController;
	 	$rules = $controllers->getRules();
		$this->assertNotEmpty($rules, 'Non sono state restituite le regole');
	}
	
	/**
	 * Verifica che venga restituito il prodotto
	 */
	public function testGetDetails() {
		$controllers = new JournalArticleController;
		$prod = $controllers->getDetails();
		$this->assertNotEmpty($prod, 'Non e\' stato restituito alcun prodotto');
	}	
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetInsertView()
	{
		$controllers = new JournalArticleController;
		$view=$controllers->getInsertView();
		$this->assertNotEmpty($view, 'Non e\' stata restituita alcuna view');
	}
	
	/**
	 * Verifica che il metodo restituisca una view
	 */
	public function testGetModifyView()
	{
	 	$controllers = new JournalArticleController;
		$view = $controllers->getModifyView();
		$this->assertNotEmpty($view, 'Non e\' stata restituita alcuna view');
	}
}

?>